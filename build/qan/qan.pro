
LANGUAGE = C++
DEFINES += QANAVA
TARGET = qanava
DESTDIR = ../
CONFIG += debug \
          warn_on \
          qt \
          thread staticlib
QT += xml
TEMPLATE = lib
HEADERS += ../../src/qanConfig.h \
		   ../../src/qanEdge.h \
           ../../src/qanGraph.h \
           ../../src/qanGraph.hpp \
           ../../src/qanGrid.h \
           ../../src/qanLayout.h \
           ../../src/qanSimpleLayout.h \
           ../../src/qanTreeLayout.h \
           ../../src/qanNode.h \
           ../../src/qanVectorf.h \
           ../../src/qanRepository.h \
           ../../src/qanTimeTree.h \
		   ../../src/qanController.h \
           ../../src/qanNodeItem.h \
           ../../src/qanEdgeItem.h \
           ../../src/qanStyle.h \
           ../../src/qanGraphView.h \
           ../../src/qanGraphModel.h \
           ../../src/qanGraphScene.h \
           ../../src/qanGridItem.h \
		   ../../src/ui/uiStyleView.h \
		   ../../src/ui/uiStyleModel.h \
		   ../../src/ui/uiNodesItemModel.h \
		   ../../src/ui/uiUtil.h

SOURCES += ../../src/qanEdge.cpp \
           ../../src/qanLayout.cpp \
           ../../src/qanSimpleLayout.cpp \
           ../../src/qanTreeLayout.cpp \
           ../../src/qanNode.cpp \
           ../../src/qanRepository.cpp \
           ../../src/qanTimeTree.cpp \
		   ../../src/qanController.cpp \
           ../../src/qanNodeItem.cpp \
           ../../src/qanEdgeItem.cpp \
           ../../src/qanStyle.cpp \
           ../../src/qanGraphView.cpp \
           ../../src/qanGraphModel.cpp \
           ../../src/qanGraphScene.cpp \
           ../../src/qanGridItem.cpp \
		   ../../src/ui/uiStyleView.cpp \
		   ../../src/ui/uiNodesItemModel.cpp \
		   ../../src/ui/uiStyleModel.cpp

macx | unix{
  QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  DEFINES += QANAVA_UNIX
}
win32{
  OBJECTS_DIR = ./Debug
}

