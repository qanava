/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laNode.cpp
// \author	Benoit Autheman (authem_b@epita.fr)
// \date	2004 February 15
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanNode.h"
#include "./qanGraph.h"


// Std headers
#include <algorithm>
#include <iterator>


namespace qan { // ::qan


/* Node Constructor/Destructor *///--------------------------------------------
Node::Node( const QString& label ) : AttrFunc( )
{
	initAttributes( 5 );
	setLabel( label );
	setType( -1 );
	VectorF v( 2 ); v( 0 ) = 0.f; v( 0 ) = 0.f;
	setPosition( v );
	setDimension( v );
}

Node::Node( const QString& label, int type ) : AttrFunc( )
{
	initAttributes( 5 );
	setLabel( label );
	setType( type );
	VectorF v( 2 ); v( 0 ) = 0.f; v( 0 ) = 0.f;
	setPosition( v );
	setDimension( v );
}
//-----------------------------------------------------------------------------



/* Node Edges Management *///--------------------------------------------------
void	Node::collectOutNodes( Node::List& outNodes )
{
	foreach ( Edge* outEdge, _outEdges )
		outNodes << &outEdge->getDst( );
	//for ( Edge::List::iterator outEdgeIter = _outEdges.begin( ); outEdgeIter != _outEdges.end( ); outEdgeIter++ )
		//outNodes << &( ( *outEdgeIter )->getDst( ) );
}

void	Node::collectInNodes( Node::List& inNodes )
{
	for ( Edge::List::iterator intEdgeIter = _inEdges.begin( ); intEdgeIter != _inEdges.end( ); intEdgeIter++ )
		inNodes << &( ( *intEdgeIter )->getSrc( ) );
}

void	Node::collectOutNodesSet( Node::Set& outNodes ) const
{
	// Add all edge destination to the out nodes set
	const Edge::List& edges = getOutEdges( );
	Edge::List::const_iterator edgeIter = edges.begin( );
	for ( ; edgeIter != edges.end( ); edgeIter++ )
		outNodes.insert( &( *edgeIter )->getDst( ) );
}

void	Node::collectInNodesSet( Node::Set& inNodes ) const
{
	// Add all edge destination to the out nodes set
	const Edge::List& edges = getInEdges( );
	Edge::List::const_iterator edgeIter = edges.begin( );
	for ( ; edgeIter != edges.end( ); edgeIter++ )
		inNodes.insert( &( *edgeIter )->getSrc( ) );
}

void			Node::getAdjacentNodesSet( Node::Set& adjacentNodes ) const
{
	Node::Set outNodes; collectOutNodesSet( outNodes );
	Node::Set inNodes; collectInNodesSet( inNodes );

	adjacentNodes.unite( outNodes );
	adjacentNodes.unite( inNodes );
}

void	Node::getNonAdjacentNodesSet( Node::Set& nonAdjacentNodes, const Node::Set& graphNodes ) const
{
	Node::Set adjacentNodes; getAdjacentNodesSet( adjacentNodes );
	nonAdjacentNodes.unite( graphNodes );
	nonAdjacentNodes.subtract( adjacentNodes );

	// Remove the node idself from to non adjacent nodes list
	nonAdjacentNodes.remove( const_cast< Node* >( this ) );
}
//-----------------------------------------------------------------------------



/* Node Attributes Management *///---------------------------------------------
void	Node::setDate( const QString& date )
{
	QDateTime dt = QDateTime::fromString( date.toAscii( ), Qt::ISODate );
	if ( dt.isValid( ) )
		setAttribute< QDateTime >( Node::DATE, dt );
}
//-----------------------------------------------------------------------------


} // ::qan

