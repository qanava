/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanEdgeItem.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 February 08
//-----------------------------------------------------------------------------


#ifndef qanEdgeItem_h
#define qanEdgeItem_h


// Qanava headers
#include "./qanNode.h"
#include "./qanStyle.h"


// QT headers
#include <QGraphicsItem>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	class NodeItem;

	class GraphItem : public QObject
	{
		Q_OBJECT

	public:

		GraphItem( Style::Manager& styleManager ) :
			_styleManager( styleManager ), _style( 0 ) { }

		GraphItem( Style::Manager& styleManager, Style* style ) :
			_styleManager( styleManager ), _style( 0 ) { setStyle( style ); }

		void					setStyle( Style* style )
		{
			if ( _style != 0 )		// Disconnect the previous style from this item slots
				_style->disconnect( SIGNAL( modified( ) ), this, SLOT( updateItem( ) ) );
			if ( style != 0 )
				connect( style, SIGNAL( modified( ) ), this, SLOT( updateItem( ) ) );
			_style = style;
		}

		Style*					getStyle( ) { return _style; }

		Style::Manager&			getStyleManager( ) { return _styleManager; }

		const Style::Manager&	getStyleManager( ) const { return _styleManager; }

		virtual	QGraphicsItem*	getGraphicsItem( ) = 0;

	protected slots:

		virtual void			updateItem( ) { }

	protected:

		Style*				_style;

		Style::Manager&		_styleManager;
	};


	class EdgeItem : public GraphItem, public QGraphicsItem
	{
		Q_OBJECT

	public:

		EdgeItem( Style::Manager& styleManager, Style* style, QGraphicsItem* parent, QGraphicsScene* scene,
				  Edge& edge, NodeItem* src, NodeItem* dst );

		virtual ~EdgeItem( );

		virtual	QGraphicsItem*	getGraphicsItem( ) { return this; }

		QRectF			boundingRect( ) const;

		void			paint( QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0 );

		virtual void	updateItem( );

		void			disconnectEdge( );

		NodeItem*		getSrc( ) { return _src; }

		NodeItem*		getDst( ) { return _dst; }

	public slots:

		void			nodeDestroyed( );

	private:

		Edge&		_edge;

		NodeItem*	_src;

		NodeItem*	_dst;

		QPointF		_sourcePoint;

		QPointF		_destinationPoint;

		bool		_hasArrow;

		int			_arrowSize;

		int			_lineType;

		int			_lineWeight;

		QRectF		_labelRect;
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qanEdgeItem_h

