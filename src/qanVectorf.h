/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laVectorF.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 February 15
//-----------------------------------------------------------------------------


#ifndef laVectorF_h
#define laVectorF_h


// Qanava headers
#include "./qanConfig.h"


// Standard headers
#include <string>
#include <list>


//-----------------------------------------------------------------------------
//! Root qanava namespace
namespace qan { // ::qan

	class VectorF
		{
		public:

			VectorF(  ) : _length( 0 ), _data( 0 ) { }

			VectorF( unsigned int length ) : _length( length ), _data( 0 ) { _data = new double[ length ]; }

			VectorF( const VectorF& v )
			{
				_length = v._length;
				_data = new double[ v._length ];
				for ( unsigned int i = 0; i < v._length; i++ )
					_data[ i ] = v._data[ i ];
			}

			~VectorF( ) { if ( _data != 0 ) delete _data; }

			double&	operator()( unsigned int i ) { return _data[ i ]; }

			const double&	operator()( unsigned int i ) const { return _data[ i ]; }

			double	operator[]( unsigned int i ) const { return _data[ i ]; }

			void	operator=( const VectorF& v )
			{
				if ( _length == v._length && _data != 0 )
				{
					for ( unsigned int i = 0; i < v._length; i++ )
						_data[ i ] = v._data[ i ];
				}
				else
				{
					//if ( _data != 0 )
					//	delete _data;
					_length = v._length;
					_data = new double[ v._length ];
					for ( unsigned int i = 0; i < v._length; i++ )
						_data[ i ] = v._data[ i ];
				}
			}

			void	operator/=( double f )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] /= f;
			}

			void	operator*=( double f )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] *= f;
			}

			void	operator+=( const VectorF& b )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] += b._data[ i ];
			}

			void	operator+=( double v )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] += v;
			}

			void	operator-=( const VectorF& b )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] -= b._data[ i ];
			}

			void	operator/=( const VectorF& b )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] /= b._data[ i ];
			}

			void	operator*=( const VectorF& b )
			{
				for ( unsigned int i = 0; i < _length; i++ )
					_data[ i ] *= b._data[ i ];
			}

			VectorF	operator-( const VectorF& b ) const
			{
				VectorF result( _length );
				for ( unsigned int i = 0; i < _length; i++ )
					result._data[ i ] = _data[ i ] - b._data[ i ];
				return result;
			}

			VectorF	operator+( const VectorF& b ) const
			{
				VectorF result( _length );
				for ( unsigned int i = 0; i < _length; i++ )
					result._data[ i ] = _data[ i ] + b._data[ i ];
				return result;
			}

			VectorF	operator/( const VectorF& b ) const
			{
				VectorF result( _length );
				for ( unsigned int i = 0; i < _length; i++ )
					result._data[ i ] = _data[ i ] / b._data[ i ];
				return result;
			}

			VectorF	operator/( double f ) const
			{
				VectorF result( _length );
				for ( unsigned int i = 0; i < _length; i++ )
					result._data[ i ] = _data[ i ] / f;
				return result;
			}

		private:

			unsigned int	_length;

			double*			_data;
		};
} // ::qan
//-----------------------------------------------------------------------------


#endif // laVectorF_h
