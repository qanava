/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanNodeItem.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 October 13
//-----------------------------------------------------------------------------


#ifndef canNodeItem_h
#define canNodeItem_h


// Qanava headers
#include "./qanNode.h"
#include "./qanStyle.h"
#include "./qanEdgeItem.h"


// QT headers
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	class AbstractNodeItem : public GraphItem
	{
		Q_OBJECT

	public:

		AbstractNodeItem( Node& node, Style::Manager& styleManager, Style* style ) :
			GraphItem( styleManager, style ),
			_node( node )
		{

		}

		virtual	~AbstractNodeItem( )
		{
		}

		//! Usually, return this casted to QGraphicsItem.
		virtual QGraphicsItem*		getGraphicsItem( ) = 0;

	public:

		Node&				getNode( ) { return _node; }

	private:

		Node&				_node;

	public:

		void				addEdge( EdgeItem* edge )
		{
			_edges << edge;
			connect( this, SIGNAL( destroyed( ) ), edge, SLOT( nodeDestroyed( ) ) );
		}

		void				removeEdge( EdgeItem* edge ) { _edges.removeAll( edge ); }

		void				updateEdges( )
		{
			foreach ( EdgeItem* edge, _edges )
				edge->updateItem( );
		}

	protected:

		QList< EdgeItem* >	_edges;

	public:

		class	AbstractFactory
		{
		public:

			AbstractFactory( int nodeType, bool defaultFactory = false ) :
				_nodeType( nodeType ),
				_defaultFactory( defaultFactory ) { };

			virtual ~AbstractFactory( ) { }

			virtual	AbstractNodeItem*	create( Node& node, Style::Manager& styleManager,
											QGraphicsItem* parent, QGraphicsScene* scene, Style* style,
											QPoint origin, const QString& label) = 0;

			int						getNodeType( ) const { return _nodeType; }

			bool					isDefaultFactory( ) const { return _defaultFactory; }

			typedef	QList< AbstractFactory* >	List;

		private:

			int				_nodeType;

			bool			_defaultFactory;
		};

		template < typename T >
		class Factory : public AbstractFactory
		{
		public:

			Factory( int nodeType, bool defaultFactory = false ) :
				AbstractFactory( nodeType, defaultFactory ) { }

			virtual AbstractNodeItem*	create( Node& node, Style::Manager& styleManager, QGraphicsItem* parent, QGraphicsScene* scene, Style* style,
												QPoint origin, const QString& label )
					{
						return new T( node, styleManager, *style, parent, scene, origin, label );
					}
		};
	};



	//! Model a rectangular node item on a QT graphics view.
	/*!
	The following style options are supported:
	<ul>
	<li> <b>'backcolor':</b> Background color, when there is no background image defined.
	<li> <b>'bordercolor':</b> Color of the item border.
	<li> <b>'backimage':</b> Background image (scaled to fit the item size).
	<li> <b>'maximumwidth':</b> Maximum width of the item, content is cropped to fit this with limit.
	<li> <b>'maximumheight':</b> Maximum height of the item, content is cropped to fit this height limit.
	<li> <b>'fontsize':</b> Base size for the font used to display the item label.
	<li> <b>'icon':</b> Display an icon centered in the left of the item.
	<li> <b>'hasshadow':</b> Set this value to false to supress the node shadow.
	</ul>
		\nosubgrouping
	*/
	class NodeItem : public AbstractNodeItem, public QGraphicsRectItem
	{
		Q_OBJECT

	public:

		NodeItem( Node& node, Style::Manager& styleManager, Style& style,
				  QGraphicsItem* parent, QGraphicsScene* scene,
				  QPointF origin, const QString& label );

		virtual ~NodeItem( );

		void			paint( QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0 );

		struct	DefaultFactory : public AbstractNodeItem::Factory< NodeItem >
		{
			DefaultFactory( ) : AbstractNodeItem::Factory< NodeItem >( -1, true ) { }
		};

	public slots:

		virtual void				updateItem( );

		virtual QGraphicsItem*		getGraphicsItem( ) { return static_cast< QGraphicsItem* >( this ); }

	private:

		QPointF				_dimension;

		QString				_label;

		QPixmap				_backPixmap;

		QColor				_shadowColor;

		double				_shadowOffset;

		QGraphicsTextItem*	_labelItem;

		QPixmap				_icon;

	protected:

		QVariant			itemChange( QGraphicsItem::GraphicsItemChange change, const QVariant& value );
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qanNodeItem_h

