/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canController.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2003 August 13
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanController.h"
#include "./qanGraphView.h"


// QT headers
#include <QMatrix>
#include <QTimer>
#include <QPixmap>
#include <QKeyEvent>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QScrollBar>


namespace qan {  // ::qan


bool	Controller::Manager::keyPressEvent( QKeyEvent* e )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->keyPressEvent( e ) )
			return true;
	}
	return false;
}

bool	Controller::Manager::mousePressEvent( QMouseEvent* e )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->mousePressEvent( e ) )
			return true;
	}
	return false;
}

bool	Controller::Manager::mouseReleaseEvent( QMouseEvent* e )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->mouseReleaseEvent( e ) )
			return true;
	}
	return false;
}

bool	Controller::Manager::mouseMoveEvent( QMouseEvent* e )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->mouseMoveEvent( e ) )
			return true;
	}
	return false;
}

bool	Controller::Manager::mouseDoubleClickEvent( QMouseEvent* e )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->mouseDoubleClickEvent( e ) )
			return true;
	}
	return false;
}

bool	Controller::Manager::wheelEvent( QWheelEvent* e )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->wheelEvent( e ) )
			return true;
	}
	return false;
}

Controller*		Controller::Manager::getController( QString name )
{
	for ( Controller::Manager::iterator controllerIter = begin( ); controllerIter != end( ); controllerIter++ )
	{
		if ( ( *controllerIter )->getName( ) == name )
			return ( *controllerIter );
	}
	return 0;
}

void	Controller::Manager::registerController( Controller* controller )
{
	if ( controller == 0 )
		return;

	push_back( controller );
	QAction* controllerAction = controller->getAction( );
	if ( controllerAction->isCheckable( ) )
		_actionGroup->addAction( controllerAction );  // Only one checkable action active at a time
}

Controller::Controller( QString name, GraphView& GraphView ) :
	_GraphView( GraphView ),
	_action( 0 ),
	_name( name )
{

}


/* PanController Constructor/Destructor *///-----------------------------------
PanController::PanController( GraphView& GraphView ) :
	Controller( "pan_controller", GraphView ),
	_keyboardNavigation( true ),
	_keyboardNavigationIntensity( 10.0 ),
	_start( 0., 0. )
{
	QAction* action = new QAction( this );
	action->setCheckable( true );
	action->setText( "Pan" );
	action->setVisible( true );
	action->setIcon( QIcon( "images/qanava_pan.png" ) );
	connect( action, SIGNAL( toggled( bool ) ), this, SLOT( toggled( bool ) ) );
	setAction( action );

	setKeyboardNavigation( true );
}

void	PanController::toggled( bool state )
{
	setMode( state ? PAN : NONE );
}
//-----------------------------------------------------------------------------



/* PanController Keyboard Management *///--------------------------------------
bool	PanController::keyPressEvent( QKeyEvent* e )
{
	getGraphView( ).setFocusPolicy( Qt::ClickFocus );
	if ( getKeyboardNavigation( ) )
	{
		switch ( e->key( ) )
		{
		case Qt::Key_Left:
			getGraphView( ).translate( ( int )-_keyboardNavigationIntensity, 0 );
			break;

		case Qt::Key_Up:
			getGraphView( ).translate( 0, ( int )-_keyboardNavigationIntensity );
			break;

		case Qt::Key_Right:
			getGraphView( ).translate( ( int )_keyboardNavigationIntensity, 0 );
			break;

		case Qt::Key_Down:
			getGraphView( ).translate( 0, ( int )_keyboardNavigationIntensity );
			break;

		default:
			e->ignore( );
			break;
		}
	}
	return false;
}

/*! To use keyboard navigation, ensure that this widget parent widget has at least a 'Click'
    focus policy (in Designer or with the QWidget::setFocusPolicy() method).
*/
void	PanController::setKeyboardNavigation( bool state )
{
	_keyboardNavigation = state;
	if ( state )
	{
		getGraphView( ).viewport( )->setFocusPolicy( Qt::StrongFocus );
		getGraphView( ).setFocusPolicy( Qt::StrongFocus );
	}
}
//-----------------------------------------------------------------------------



/* PanController Mouse Management *///-----------------------------------------
bool	PanController::mousePressEvent( QMouseEvent* e )
{
	// Navigation must not occurs if a node is under the mouse
	QPointF p = getGraphView( ).mapToScene( e->pos( ) );
	QList< QGraphicsItem* > items = getGraphView( ).scene( )->items( p );
	if ( items.size( ) == 0 )
	{										// TODO: prendre les elements de grille en compte (et tous ceux qui ne sont pas manipulables)
		if ( getMode( ) == PAN )
		{
			_start = e->pos( );
			getGraphView( ).viewport( )->setCursor( QCursor( Qt::SizeAllCursor ) );
			setState( PANNING );
			return true;
		}
	}

	return false;
}

bool	PanController::mouseReleaseEvent( QMouseEvent* e )
{
	getGraphView( ).viewport( )->setCursor( QCursor( Qt::ArrowCursor ) );

	if ( getMode( ) == PAN && getState( ) == PANNING )
		setState( NONE );  // Stop panning until next click

	return false;
}

bool	PanController::mouseMoveEvent( QMouseEvent* e )
{
	if ( getMode( ) == PAN && getState( ) == PANNING )
	{
		QPointF p = e->pos( );  // No mapping, we move scrollbars in their coordinate system
		//QPointF p = getGraphView( ).mapToScene( e->pos( ) );

		double accel = 1.0;
		double dx = ( _start.x( ) - p.x( ) ) * accel;
		double dy = ( _start.y( ) - p.y( ) ) * accel;

		int valueY = ( int )( getGraphView( ).verticalScrollBar( )->value( ) + dy );
		getGraphView( ).verticalScrollBar( )->setValue( valueY );
		int valueX = ( int )( getGraphView( ).horizontalScrollBar( )->value( ) + dx );
		getGraphView( ).horizontalScrollBar( )->setValue( valueX );

		_start = p;
		return true;
	}
	return false;
}
//-----------------------------------------------------------------------------



/* ZoomWindowController Constructor/Destructor *///-------------------------------
ZoomWindowController::ZoomWindowController( GraphView& GraphView ) :
	Controller( "zoom_window_controller", GraphView ),
	_start( 0., 0. ),
	_zoomRectItem( 0 )
{
	QAction* action = new QAction( this );
	action->setCheckable( true );
	action->setText( "Zoom Window" );
	action->setVisible( true );
	action->setIcon( QIcon( "images/qanava_zoomwindow.png" ) );
	connect( action, SIGNAL( toggled( bool ) ), this, SLOT( toggled( bool ) ) );
	setAction( action );

	_zoomRectItem = new QGraphicsRectItem( QRectF( 0., 0., 50., 50. ), 0, getGraphView( ).scene( ) );
	_zoomRectItem->setZValue( 100. );
	QPen p;
	p.setStyle( Qt::DotLine );
	p.setWidth( 1 );
	p.setColor( QColor( 55, 55, 55 ) );
	_zoomRectItem->setPen( p );
	_zoomRectItem->hide( );
}

void	ZoomWindowController::toggled( bool state )
{
	setMode( state ? ZOOM : NONE );
}
//-----------------------------------------------------------------------------



/* ZoomWindowController Zooming Management *///--------------------------------
bool	ZoomWindowController::mousePressEvent( QMouseEvent* e )
{
	// Navigation must not occurs if a node is under the mouse
	QPointF p = getGraphView( ).mapToScene( e->pos( ) );
	QList< QGraphicsItem* > items = getGraphView( ).scene( )->items( p );
	if ( items.size( ) == 0 )
	{										// TODO: prendre les elements de grille en compte (et tous ceux qui ne sont pas manipulables)
		if ( getMode( ) == ZOOM )
		{
			_start = getGraphView( ).mapToScene( e->pos( ) );
			setState( ZOOMING );
			return true;
		}
	}

	return false;
}

bool	ZoomWindowController::mouseReleaseEvent( QMouseEvent* e )
{
	if ( getMode( ) == ZOOM && getState( ) == ZOOMING )
	{
		getGraphView( ).fitInView( _zoomRectItem->rect( ), Qt::KeepAspectRatioByExpanding );
		_zoomRectItem->hide( );
		setState( NONE );  // Stop zooming until next click
		return true;
	}

	return false;
}

bool	ZoomWindowController::mouseMoveEvent( QMouseEvent* e )
{
	if ( getMode( ) == ZOOM && getState( ) == ZOOMING && _zoomRectItem != 0 )
	{
		QPointF p = getGraphView( ).mapToScene( e->pos( ) );

		QRectF r( _start.x( ), _start.y( ),
				  p.x( ) - _start.x( ), p.y( ) - _start.y( ) );
		_zoomRectItem->setRect( r );
		_zoomRectItem->show( );
		return true;
	}
	return false;
}
//-----------------------------------------------------------------------------



/* ZoomController Constructor/Destructor *///----------------------------------
ZoomController::ZoomController( GraphView& GraphView ) :
	Controller( "zoom_controller", GraphView ),
	_actionZoomIn( new QAction( this ) ),
	_actionZoomOut( new QAction( this ) )
{
	QAction* action = new QAction( this );
	action->setCheckable( true );
	action->setText( "Zoom" );
	action->setVisible( true );
	action->setIcon( QIcon( "images/qanava_zoom.png" ) );
	setAction( action );

	_actionZoomIn->setText( "Zoom In" );
	_actionZoomIn->setVisible( true );
	_actionZoomIn->setIcon( QIcon( "images/qanava_zoomin.png" ) );
	connect( _actionZoomIn, SIGNAL( triggered( bool ) ), this, SLOT( zoomIn( bool ) ) );

	_actionZoomOut->setText( "Zoom Out" );
	_actionZoomOut->setVisible( true );
	_actionZoomOut->setIcon( QIcon( "images/qanava_zoomout.png" ) );
	connect( _actionZoomOut, SIGNAL( triggered( bool ) ), this, SLOT( zoomOut( bool ) ) );
}
//-----------------------------------------------------------------------------



/* ZoomController Zooming Management *///--------------------------------------
void		ZoomController::zoomIn( bool state )
{
	getGraphView( ).setZoom( getGraphView( ).getZoom( ) + 0.2 );
}

void		ZoomController::zoomOut( bool state )
{
	getGraphView( ).setZoom( getGraphView( ).getZoom( ) - 0.2 );
}

bool		ZoomController::wheelEvent( QWheelEvent* e )
{
	return false;
}

QAction*	ZoomController::getAction( QString name )
{
	if ( name == "zoom_in" )
		return _actionZoomIn;
	else if ( name == "zoom_out" )
		return _actionZoomOut;

	return 0;
}
//-----------------------------------------------------------------------------



/* Selection Controller Management *///------------------------------------------
/*SelectionController::SelectionController( GraphView& GraphView ) :
	Controller( GraphView )
{
}

bool	SelectionController::mouseDoubleClickEvent( QMouseEvent* e )
{*/
	// FIXME
/*	QPointF p = getGraphView( ).mapToScene( e->pos( ) );
	Item::List collisions;
	getGraphView( ).getCanvas( )->getCollisions( p, collisions );
	for ( Item::List::iterator itemIter = collisions.begin( ); itemIter != collisions.end( ); itemIter++ )
	{
		Item* item = *itemIter;
		if ( getGraphView( ).getCanvas( )->isFreed( item ) )
			break;

		Node* node = getGraphView( ).getGraphicItemNode( item );
		if ( node != 0 )
			emit nodeSelected( node, p );
	}*/

/*	return false;
}*/
//-----------------------------------------------------------------------------



/* Tooltip Controller Management *///--------------------------------------------
/*TooltipController::TooltipController( GraphView& GraphView ) :
	Controller( GraphView ),
	//_tipItem( 0 ),
	_tipPos( 0, 0 ),
	_timer( 0 )
{
	_timer = new QTimer( this );
	connect( _timer, SIGNAL( timeout( ) ), SLOT( tipTimeout( ) ) );
}*/

/*void	TooltipController::tipTimeout( )
{*/
	// FIXME
	// Don't show tooltips in case of error or if another controller is already active
/*	if ( _tipItem == 0 || getGraphView( ).getControllerManager( ).isControllerActive( ) )
	{
		_timer->stop( );
		return;
	}

	// Test if the mouse is still on the item that has started the tooltip demand (tooltip delay) milliseconds ago
	Item::List collisions;
	getGraphView( ).getCanvas( )->getCollisions( _tipPos, collisions );
	for ( Item::List::iterator itemIter = collisions.begin( ); itemIter != collisions.end( ); itemIter++ )
	{
		Item* item = *itemIter;

		if ( item == _tipItem )
		{
			Node* node = getGraphView( ).getGraphicItemNode( _tipItem );
			if ( node != 0 )
			{
				// Compute the real screen viewport coordinates
				QPoint viewportTipPosContent = getGraphView( ).mapFromCanvas( _tipPos );
				emit nodeTipped( node, getGraphView( ).viewport( )->mapToGlobal( viewportTipPosContent ) );
			}
		}
	}
	_timer->stop( );
	_tipItem = 0;*/
//}

//bool	TooltipController::mouseMoveEvent( QMouseEvent* e )
//{
	// FIXME
/*	QPointF p = getGraphView( ).mapToScene( e->pos( ) );
	_tipPos = p;

	// If no button is pressed, there is no manipulation or navigation operation going on, so tooltip can be displayed
	if ( e->button() != Qt::LeftButton )
	{
		Item::List collisions;
		getGraphView( ).getCanvas( )->getCollisions( _tipPos, collisions );
		for ( Item::List::iterator itemIter = collisions.begin( ); itemIter != collisions.end( ); itemIter++ )
		{
			Item* item = *itemIter;
			if ( item == _tipItem && _timer->isActive( ) )  // Avoid the timer to be restarted when moving in the same item
				break;

			_tipPos = p;
			if ( getGraphView( ).getCanvas( )->isFreed( item ) )
				break;

			Node* node = getGraphView( ).getGraphicItemNode( item );
			if ( node != 0 )
			{
				_timer->start( 800 );
				_tipItem = item;
				return true;
			}
			break; // Take just the first intersecting item to avoid deep z selection
		}
	}
	else
	{
		// There is a complex navigation or manipulation operation going on, don't display tooltips
		_timer->stop( );
		_tipItem = 0;
	}
	*/
//	return false;
//}

//bool	TooltipController::mouseDoubleClickEvent( QMouseEvent* e )
//{
	// Avoid interference with other controllers (such as simultaneous tip and double click node selection)
	// FIXME
	/*_timer->stop( );
	_tipItem = 0;*/
//	return false;
//}
//-----------------------------------------------------------------------------


} // ::qan

