/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laNode.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 February 15
//-----------------------------------------------------------------------------


#ifndef laNode_h
#define laNode_h


// Qanava headers
#include "./qanEdge.h"
#include "./qanVectorf.h"


// Standard headers
#include <cassert>


// QT headers
#include <QList>
#include <QSet>
#include <QDateTime>


//-----------------------------------------------------------------------------
//! Root qanava namespace
namespace qan { // ::qan

		//! Model a node in a standard weighted and directed graph.
		/*!
			\nosubgrouping
		*/
		class Node : public AttrFunc
		{
			/*! \name Node Constructor/Destructor *///-------------------------
			//@{
		public:

			//! Node constructor with label initialisation.
			Node( const QString& label );

			//! Node constructor with label and type initialisation.
			Node( const QString& label, int type );

			//! Node destructor.
			~Node( ) { }

		private:

			Node( const Node& n );
			//@}
			//-----------------------------------------------------------------



			/*! \name Node Edges Management *///-------------------------------
			//@{
		public:

			//! Typedef for a QT list of pointer on Node.
			typedef QList< Node* >	List;

			//! Typedef for a QT set of pointer on Node.
			typedef QSet< Node* >	Set;

			//! Get a list of all nodes pointing to this node.
			const Edge::List&	getInEdges( ) const { return _inEdges; }

			//! Get a list of all nodes pointing to this node.
			Edge::List&			getInEdges( ) { return _inEdges; }

			//! Get a list of all node pointed by this node.
			const Edge::List&	getOutEdges( ) const { return _outEdges; }

			//! Get a list of all node pointed by this node.
			Edge::List&			getOutEdges( ) { return _outEdges; }

			//! Collect a list of this node sub nodes.
			void				collectOutNodes( Node::List& outNodes );

			//! Collect a list of this node in nodes.
			void				collectInNodes( Node::List& outNodes );

			//! Collect a set of unique nodes pointed by this node.
			void				collectOutNodesSet( Node::Set& outNodes ) const;

			//! Collect a set of unique nodes referencing this node.
			void				collectInNodesSet( Node::Set& nodes ) const;

			//! Add an in edge.
			void				addInEdge( Edge& edge ) { _inEdges << &edge; }

			//! Add an out edge.
			void				addOutEdge( Edge& edge ) { _outEdges << &edge; }

			//! Get node in degree.
			unsigned int		getInDegree( ) const { return _inEdges.size( ); }

			//! Get node out degree.
			unsigned int		getOutDegree( ) const { return _outEdges.size( ); }

			//! Get a list of nodes adjacent to this (all in and out nodes, without this).
			void				getAdjacentNodesSet( Node::Set& nodes ) const;

			//! Get a list of nodes non adjacent to this (all nodes minus the adjacent node set collected with getAdjacentNodesSet()).
			void				getNonAdjacentNodesSet( Node::Set& nonAdjacentNodes, const Node::Set& graphNodes ) const;

			//! Return true if this node is a "leaf" (ie has no out edges).
			bool				isLeaf( ) const { return _outEdges.size( ) == 0; }

		private:

			//! Input edges.
			Edge::List			_inEdges;

			//! Output edges.
			Edge::List			_outEdges;
			//@}
			//-----------------------------------------------------------------



			/*! \name Node Property Management *///----------------------------
			//@{
		public:

			//! Attribute role.
			enum Role
			{
				TYPE			= 1,
				LABEL			= 2,
				POSITION		= 3,
				DIMENSION		= 4,
				DATE			= 5,
				USER			= 6
			};

			enum { StdAttributeCount = 5 };

			//! Get this node label.
			const QString&	getLabel( ) const { return *getAttribute< QString >( Node::LABEL ); }

			//! Set this node label.
			void			setLabel( const QString& label ) { setAttribute< QString >( Node::LABEL, label ); }

			//! Set this node's user defined type.
			void			setType( int type ) { setAttribute< int >( Node::TYPE, type ); }

			//! Get this node's user defined type.
			int				getType( ) const { return *getAttribute< int >( Node::TYPE ); }

			//! .
			VectorF&		getPosition( ) { return *getAttribute< VectorF >( Node::POSITION ); }

			//! .
			const VectorF&	getPosition( ) const { return *getAttribute< VectorF >( Node::POSITION ); }

			//! .
			void			setPosition( VectorF& position ) { setAttribute< VectorF >( Node::POSITION, position ); }

			//! .
			void			setPosition( float x, float y )
			{
				VectorF& position = getPosition( );
				position( 0 ) = x; position( 1 ) = y;
			}

			const VectorF&	getDimension( ) const { return *getAttribute< VectorF >( Node::DIMENSION ); }

			void			setDimension( const VectorF& dimension ) { setAttribute< VectorF >( Node::DIMENSION, dimension ); }

			void			setDimension( float x, float y )
			{
				VectorF& dimension = *getAttribute< VectorF >( Node::DIMENSION );
				dimension( 0 ) = x; dimension( 1 ) = y;
			}

			//! Set the node date from a text string with the Posix Time format (ex: 2002-Jan-01 10:00:01).
			void				setDate( const QString& date );

			//! Return the node date under Posix Time format (0 if the date is undefined).
			const QDateTime*	getDate( ) const { return getAttribute< QDateTime >( Node::DATE ); }
			//@}
			//-----------------------------------------------------------------
		};
} // ::qan
//-----------------------------------------------------------------------------


#endif // laNode_h
