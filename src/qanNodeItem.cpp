/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanNodeItem.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 October 13
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanNodeItem.h"


// QT headers
#include <QFont>
#include <QPainter>
#include <QPixmap>
#include <QGraphicsTextItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsTextItem>


namespace qan {	// ::qan


/* Node Constructor/Destructor *///--------------------------------------------
/*!
	The following style options are supported:
	<ul>
	<li> <b>'backcolor':</b> Background color, when there is no background image defined.
	<li> <b>'bordercolor':</b> Color of the item border.
	<li> <b>'backimage':</b> Background image (scaled to fit the item size).
	<li> <b>'maximumwidth':</b> Maximum width of the item, content is cropped to fit this with limit.
	<li> <b>'maximumheight':</b> Maximum height of the item, content is cropped to fit this height limit.
	<li> <b>'fontsize':</b> Base size for the font used to display the item label.
	<li> <b>'icon':</b> Display an icon centered in the left of the item.
	<li> <b>'hasshadow':</b> Set this value to false to supress the node shadow.
	</ul>

	An item with an empty style is transparent with no background nor border.

	\param origin	Uper left corner of the rectangular item.
	\param label	Text label to be displayed in the node text area (can be multi line HTML tagged text).
	\param style	Advanced style to pass optionnal display parameters.
 */
NodeItem::NodeItem( Node& node, Style::Manager& styleManager, Style& style,
		    QGraphicsItem* parent, QGraphicsScene* scene,
		    QPointF origin, const QString& label ) :
	AbstractNodeItem( node, styleManager, &style ),
	QGraphicsRectItem( parent, scene ),
	_dimension( 170.0, 45.0 ),
	_label( label ),
	_shadowColor( ),
	_shadowOffset( 1.0 ),
	_labelItem( 0 )
{
	setRect( origin.x( ), origin.y( ), _dimension.x( ), _dimension.y( ) );
	setFlag( QGraphicsItem::ItemIsMovable );
	setZValue( 2.0 );

	updateItem( );
}

NodeItem::~NodeItem( )
{
	foreach ( EdgeItem* edge, _edges )
		edge->disconnectEdge( );
}

void	NodeItem::paint( QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget )
{
	{
		/*QBrush b;
		b.setStyle( Qt::SolidPattern );
		b.setColor( _shadowColor );
		painter->setBrush( b );

		QPen p( Qt::red );
		painter->setPen( p );

		QRectF r = boundingRect( );
		painter->drawRoundRect( r );*/
	}

	// Draw background pixmap
	if ( !_backPixmap.isNull( ) )
	{
		QBrush b = brush( );
		b.setStyle( Qt::NoBrush );
		setBrush( b );	// Remove default backcolor (usefull if we have an alpha channel)

		QRectF backRect = boundingRect( );
		QRectF srcRect( 0., 0., _backPixmap.width( ), _backPixmap.height( ) );
		painter->drawPixmap( backRect, _backPixmap, srcRect );
	}

	QGraphicsRectItem::paint( painter, option, widget );

	// Draw the item icon centered vertically
	if ( !_icon.isNull( ) )
	{
		QPointF iconPos( 1.0, 1.0 );
		if ( _dimension.y( ) > _icon.height( ) )
			iconPos.ry( ) += ( _dimension.y( ) - _icon.height( ) ) / 2.;
		painter->drawPixmap( iconPos, _icon );
	}
}

void	NodeItem::updateItem( )
{
	if ( getStyle( ) == 0 )
		return;

	QColor backColor = QColor( 255, 255, 255 );

	if ( !getStyle( )->has( "nobackground" ) )
	{
		QBrush b = brush( );
		b.setStyle( Qt::NoBrush );
		setBrush( b );
	}

	if ( getStyle( )->has( "backcolor" ) )
	{
		backColor = getStyle( )->getColor( "backcolor" );
		QBrush b = brush( );
		b.setStyle( Qt::SolidPattern );
		b.setColor( backColor );
		setBrush( b );
	}
	else
		setBrush( QColor( 255, 255, 255 ) );

	QColor borderColor = QColor( 0, 0, 0 );
	if ( getStyle( )->has( "bordercolor" ) )
	{
		borderColor = getStyle( )->getColor( "bordercolor" );
		setPen( borderColor );
	}

	// Compute the _label size once it is laid out as rich text html
	if ( _label.size( ) > 0 )
	{
		QFont font;
		if ( getStyle( )->has( "fontsize" ) )
		{
			int fontSize = getStyle( )->getT< int >( "fontsize" );
			font.setPointSize( fontSize > 0 ? fontSize : 11 );
		}
		_labelItem = new QGraphicsTextItem( this, scene( ) );
		_labelItem->setHtml( _label );
		_labelItem->setFont( font );
	}


	// Updating node geometry and content
	{
		// Check for item maximum dimension (if specified)
		double maximumWidth = -1.;
		double maximumHeight = -1.;
		if ( getStyle( )->has( "maximumwidth" ) )
			maximumWidth = getStyle( )->getT< int >( "maximumwidth" );
		if ( getStyle( )->has( "maximumheight" ) )
			maximumHeight = getStyle( )->getT< int >( "maximumheight" );

		// Compute the item height according to the _label size once formatted and displayed
		if ( _labelItem != 0 /*_labelDocument != 0 && _labelLayout != 0*/ )
		{
			// Do not resize the item larger than its maximum allowed size
			double textLayoutWidth = _labelItem->boundingRect( ).width( ) + 2.;
			double textLayoutHeight = _labelItem->boundingRect( ).height( ) + 2.;
			_dimension.setX( maximumWidth  > 0. ? std::min( textLayoutWidth, maximumWidth ) : textLayoutWidth );
			_dimension.setY( maximumHeight > 0. ? std::min( textLayoutHeight, maximumHeight ) : textLayoutHeight );
		}

		// Resize the whole item to fit (eventual) icon size
		if ( getStyle( )->has( "icon" ) )
		{
			QImage icon = getStyleManager( ).getImage( getStyle( )->getImageName( "icon" ) );
			if ( !icon.isNull( ) )
				_icon = QPixmap::fromImage( icon, Qt::OrderedAlphaDither );
		}

		if ( !_icon.isNull( ) )
		{
			double w = _dimension.x( ) + _icon.width( ) + 1.;
			_dimension.setX( maximumWidth != -1 ? std::min( w, maximumWidth ) : w );

			double h = std::max( _icon.height( ) + 1., _dimension.y( ) );
			_dimension.setY( maximumHeight != -1 ? std::min( h, maximumHeight ) : h );

			double textMarginX = 2;
			double textMarginY = 0;
			double textX = textMarginX;
			if ( !_icon.isNull( ) )    // Draw the text right of the icon
				textX += _icon.width( );
			double textY = textMarginY;

			QRectF clipRect( textX, textY, _dimension.x( ) - textMarginX, _dimension.y( ) - textMarginY );
			_labelItem->translate( textX, textY );
		}

		// Update the background image
		if ( getStyle( )->has( "backimage" ) )
		{
			QImage backImage = getStyleManager( ).getImage( getStyle( )->getImageName( "backimage" ) );
			if ( !backImage.isNull( ) )
			{
				QImage image = backImage.scaled( ( int )_dimension.x( ) - 1, ( int )_dimension.y( ) - 1 );  // -1 for the border
				if ( !image.isNull( ) )
					_backPixmap = QPixmap::fromImage( image, Qt::OrderedAlphaDither );
			}
		}

		if ( getStyle( )->has( "hasshadow" ) && getStyle( )->getT< bool >( "hasshadow" ) )
		{
			if ( getStyle( )->has( "shadowcolor" ) )
				_shadowColor = getStyle( )->getColor( "shadowcolor" );
			else
				_shadowColor = QColor( 105, 105, 105 );
			if ( getStyle( )->has( "shadowoffset" ) )
				_shadowOffset = getStyle( )->getT< int >( "shadowoffset" );
			else
				_shadowOffset = 3.;

			// Setup the two rectangle sub items who are modelling shadow
			// Two rect are needed, because a unique subitem modelling shadow cannot have
			// a zvalue inferior to the rect item modelling the node (BTW, it may be faster
			// to render two small rect than a big one)
			QGraphicsRectItem* shadowBottom = new QGraphicsRectItem( this, scene( ) );
			shadowBottom->setRect( _shadowOffset, _dimension.y( ) + 1.0, _dimension.x( ), _shadowOffset );
			QBrush b;
			b.setStyle( Qt::SolidPattern );
			b.setColor( _shadowColor );
			shadowBottom->setBrush( b );
			shadowBottom->setPen( QPen( _shadowColor ) );

			QGraphicsRectItem* shadowRight = new QGraphicsRectItem( this, scene( ) );
			shadowRight->setRect( _dimension.x( ) + 1.0, _shadowOffset, _shadowOffset, _dimension.y( ) + 1.0 );
			shadowRight->setBrush( b );
			shadowRight->setPen( QPen( _shadowColor ) );
		}
		else
			_shadowColor = QColor( ); // Invalid color since there is no shadow

		// Set item geometry
		setRect( 0., 0., _dimension.x( ), _dimension.y( ) );
		getNode( ).setDimension( _dimension.x( ), _dimension.y( ) );
	}
}

QVariant	NodeItem::itemChange( GraphicsItemChange change, const QVariant& value )
{
	if ( change == ItemPositionChange || change == ItemMatrixChange )
		updateEdges( );
	return QGraphicsItem::itemChange( change, value );
}
//-----------------------------------------------------------------------------

} // ::qan

