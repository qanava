/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanRepository.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 December 23
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanRepository.h"


// Standard headers
#include <iostream>
#include <fstream>
#include <sstream>


// QT headers
#include <QFile>
#include <QRegExp>
#include <QColor>


namespace qan { // ::qan


/* Graph Serialization Management *///-----------------------------------------
void	PajekRepository::load( Graph* graph )
{
	std::ifstream	netFile( getName( ).c_str( ) );
	if ( !netFile.is_open( ) )
		return;

	Mode mode = UNDEFINED;
	while ( !netFile.eof( ) )
	{
		std::string line;
		std::getline( netFile, line ); // Parse the file line by line

		std::string header( "" );
		{
			std::stringstream lineStream;
			lineStream << line;
			lineStream >> header;
		}

		Mode oldMode = mode;
		if ( header == "*Vertices" )
			mode = VERTICES;
		else if ( header == "*Arcs" )
			mode = ARCS;
		else if ( header == "*Edges" )
			mode = EDGES;
		if ( oldMode != mode )
			continue;     // A header line has just been read, jump to the next line

		std::stringstream lineStream;
		lineStream << line;

		switch ( mode )
		{
		case VERTICES:
			{
				int nodeId( -1 );
				lineStream >> nodeId;

				std::string nodeName( "" );
				lineStream >> nodeName;

				if ( ( nodeId > 0 ) && nodeName.size( ) > 0 )
				{
					graph->insertNode( QString( nodeName.c_str( ) ) );
				}
			}
			break;

		case ARCS:
			{
				int nodeSrcId( -1 );
				lineStream >> nodeSrcId;

				int nodeDstId( -1 );
				lineStream >> nodeDstId;

				if ( nodeSrcId > 0 && nodeDstId > 0 )
				{
					Node* nodeSrc = graph->findNode( nodeSrcId - 1 );
					Node* nodeDst = graph->findNode( nodeDstId - 1 );
					if ( nodeSrc != 0 && nodeDst != 0 )
						graph->addEdge( *nodeSrc, *nodeDst );
				}
			}
			break;

		case EDGES:
			{
				int nodeSrcId( -1 );
				lineStream >> nodeSrcId;

				int nodeDstId( -1 );
				lineStream >> nodeDstId;

				if ( nodeSrcId > 0 && nodeDstId > 0 )
				{
					Node* nodeSrc = graph->findNode( nodeSrcId - 1 );
					Node* nodeDst = graph->findNode( nodeDstId - 1 );
					if ( nodeSrc != 0 && nodeDst != 0 )
						graph->addEdge( *nodeSrc, *nodeDst );
				}
			}
			break;

		case UNDEFINED:
		default:
			break;
		}
	}
	netFile.close( );
	graph->generateRootNodes( );
}

void	PajekRepository::save( Graph* graph )
{

}
//-----------------------------------------------------------------------------


/* Graph Serialization Management *///-----------------------------------------
void	GraphvizRepository::load( Graph* graph )
{

}

void	GraphvizRepository::save( Graph* graph )
{
	std::ofstream ofs;
	ofs.open( getName( ).c_str( ) );
	if ( !ofs.is_open( ) )
		return;

	ofs << "graph g {\n";
	ofs << "\t center=true; \n";
	ofs << "#\t page=\"18,18\"; \n";
	ofs << "\t size=\"20,20\"; \n";
	ofs << "#\t ratio=compress; \n";
	ofs << "#\t concentrate=true; \n";
	ofs << "\t overlap=true; \n";
	ofs << "\t orientation=paysage; \n";

	ofs << "\t node [ shape=box, style=filled, fontsize=10, height=0.2, width=0.4 ];\n";
	ofs << "\t edge [ len=1.5 ];\n";

	// Dump nodes
	Node::List::iterator nodeIter = graph->getNodes( ).begin( );
	for ( int n = 0; nodeIter != graph->getNodes( ).end( ); nodeIter++ )
	{
		Node* node = *nodeIter;
		//ofs << "\t" << n++/*node->getId( )*/ << " [label=\"" << node->getLabel( ).c_str( ) << "\"];\n";
		assert( false ); // FIXME
	}

	// Dump nodes edges
	for ( nodeIter = graph->getNodes( ).begin( ); nodeIter != graph->getNodes( ).end( ); nodeIter++ )
	{
		Node* node = *nodeIter;
		const Edge::List& edges = node->getOutEdges( );
		Edge::List::const_iterator edgeIter = edges.begin( );
		for ( ; edgeIter != edges.end( ); edgeIter++ )
		{
			//Edge* edge = *edgeIter;
			ofs << "\t" << 0/*edge->getSrc( ).getId( )*/ << " -- " << 0/*edge->getDst( ).getId( )*/ << "\n";
		}
	}
	ofs << "}\n";
	ofs.close( );
}
//-----------------------------------------------------------------------------


/* GML Graph Serialization Management *///-------------------------------------
GMLRepository::GMLRepository( const std::string& name ) :
	Repository( name )
{

}

void	GMLRepository::load( Graph* graph )
{
	if ( getName( ).size( ) <= 0 )
		return;

	QFile file(	getName( ).c_str( ) );
	if ( !file.open( QFile::ReadOnly | QFile::Text ) )
        return;

    QString errorStr( "" );
    int errorLine = -1;
    int errorColumn = -1;

	QDomDocument domDocument;
	if ( !domDocument.setContent( &file, true, &errorStr, &errorLine, &errorColumn ) )
        return;

	QDomElement root = domDocument.documentElement( );
	if ( root.tagName( ) != "graphml" )
		return;

	QDomElement graphChild = root.firstChildElement( "graph" );
	if ( !graphChild.isNull( ) )
		parseGraph( domDocument, graphChild, graph );
}

void	GMLRepository::parseGraph( QDomDocument domDocument, QDomElement element, Graph* graph )
{
	if ( element.tagName( ) != "graph" )
		return;

	// Parse nodes
	QDomNodeList nodes = element.elementsByTagName( "node" );
	for( unsigned int n = 0; n < nodes.length( ); n++ )
    {
		QDomElement node = nodes.item( n ).toElement( );
		if ( node.isNull( ) )
			continue;
		QString nodeId = node.attribute( "id" );
		Node* laNode = graph->insertNode( nodeId );


		// Parse data elements
		QDomNodeList dataNodes = node.elementsByTagName( "data" );
		for ( unsigned int dn = 0; dn < dataNodes.length( ); dn++ )
		{
			QDomElement dataNode = dataNodes.item( dn ).toElement( );
			if ( dataNode.isNull( ) )
				continue;

			QString dataKey = dataNode.attribute( "key" );
			QString	dataText = dataNode.text( );

			if ( dataKey.length( ) <= 0 )
				continue;

			if ( graph->hasAttribute( dataKey ) == -1 )
				graph->addAttribute< QString >( dataKey, QString( "" ) );
			graph->setAttribute< QString >( laNode, dataKey, dataText );
		}

		// Parse styles
		Style* laStyle = parseStyle( node );
		if ( laNode != 0 && laStyle != 0 )
			graph->getM( ).applyStyle( laNode, laStyle );
    }

	graph->generateRootNodes( );

	// Parse edges
	QDomNodeList edges = element.elementsByTagName( "edge" );
	for( unsigned int e = 0; e < edges.length( ); e++ )
    {
		QDomElement edge = edges.item( e ).toElement( );
		if ( edge.isNull( ) )
			continue;
		QString edgeSource = edge.attribute( "source" );
		QString edgeTarget = edge.attribute( "target" );


		// Parse data elements
		double weight = 0.;
		QString label;
		QDomNodeList dataNodes = edge.elementsByTagName( "data" );
		for ( unsigned int dn = 0; dn < dataNodes.length( ); dn++ )
		{
			QDomElement dataNode = dataNodes.item( dn ).toElement( );
			if ( dataNode.isNull( ) )
				continue;

			QString dataKey = dataNode.attribute( "key" );
			QString	dataText = dataNode.text( );

			if ( dataKey.length( ) <= 0 )
				continue;

			if ( dataKey == "weight" )
				weight = dataText.toDouble( );
			if ( dataKey == "label" )
				label = dataText;
		}

		if ( ( edgeSource.length( ) > 0 ) && ( edgeTarget.length( ) > 0 ) )
		{
			Node* laSrc = graph->findNode( edgeSource );
			Node* laDst = graph->findNode( edgeTarget );
			if ( ( laSrc != 0 ) && ( laDst != 0 ) )
			{
				qan::Edge* laEdge = graph->addEdge( *laSrc, *laDst, weight );
				if ( !label.isEmpty( ) )
					laEdge->setLabel( label );

				// Parse styles
				Style* laStyle = parseStyle( edge );
				if ( laEdge != 0 && laStyle != 0 )
					graph->getM( ).applyStyle( laEdge, laStyle );
			}
		}

		//break;
	}

	graph->generateRootNodes( );
}

qan::Style*		GMLRepository::parseStyle( QDomElement& node )
{
	QDomNodeList styleNodes = node.elementsByTagName( "style" );
	if ( styleNodes.size( ) <= 0 )
		return 0;
	qan::Style* style = new Style( "" );
	for ( unsigned int dn = 0; dn < styleNodes.length( ); dn++ )
	{
		QDomElement styleNode = styleNodes.item( dn ).toElement( );
		if ( styleNode.isNull( ) )
			continue;

		QString attributeName = styleNode.attribute( "attribute" );
		QString attributeType = styleNode.attribute( "type" );
		QString	attributeValue = styleNode.attribute( "value" );

		if ( attributeName.length( ) <= 0 )
			continue;
		if ( attributeType == "number" && QRegExp( "[0-9]*" ).exactMatch( attributeValue ) )
			style->addT< int >( attributeName, attributeValue.toInt( ) );
		else if ( attributeType == "bool" && QRegExp( "true" ).exactMatch( attributeValue ) )
			style->addT< bool >( attributeName, true );
		else if ( attributeType == "bool" && QRegExp( "false" ).exactMatch( attributeValue ) )
			style->addT< bool >( attributeName, false );
		else if  ( attributeType == "color" && QRegExp( "#[a-f0-9]{6}" ).exactMatch( attributeValue ) )
		{
			QColor color( attributeValue );
			style->addT< QColor >( attributeName, color );
		}
		else if ( attributeType == "image" )
			style->addImageName( attributeName, attributeValue );
		else
			style->addT< QString >( attributeName, attributeValue );
	}
	return style;
}

void	GMLRepository::save( Graph* graph )
{

}
//-----------------------------------------------------------------------------


} // ::qan

