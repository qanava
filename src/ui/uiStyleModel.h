/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Mysire software.
//
// \file	uiStyleItemModel.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2006 January 04
//-----------------------------------------------------------------------------


#ifndef uiStyleItemModel_h
#define uiStyleItemModel_h


// Qanava headers
#include "../qanGraph.h"
#include "../qanStyle.h"


// QT headers
#include <QItemDelegate>
#include <QModelIndex>
#include <QAbstractItemModel>


//-----------------------------------------------------------------------------
namespace qan // ::qan
{
    namespace ui // ::qan::ui
    {
		//! .
		/*!
			\nosubgrouping
		*/
		class StyleItemModel : public QAbstractItemModel
		{
			Q_OBJECT

		public:

			StyleItemModel( Style& style, QObject *parent = 0 );

		private:

			Style&	_style;

			/*! \name Qt Model Interface *///----------------------------------
			//@{
		public:

			//! Define the article attribute offset (row) in table.
			enum AttributeOffset
			{
				TITLE	= 0,
				AUTHOR	= 1,
				SOURCE	= 2,
				SUB		= 3,
				SUP		= 4
			};

			virtual QVariant data( const QModelIndex &index, int role ) const;

			virtual bool hasChildren( const QModelIndex & parent = QModelIndex( ) ) const;

			virtual Qt::ItemFlags flags( const QModelIndex &index ) const;

			virtual QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

			virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;

			virtual QModelIndex parent( const QModelIndex &index ) const;

			virtual int rowCount( const QModelIndex& parent = QModelIndex( ) ) const;

			virtual int columnCount( const QModelIndex& parent = QModelIndex( ) ) const;
			//@}
			//-----------------------------------------------------------------
		};
    }  // ::qan::ui
} // ::qan
//-----------------------------------------------------------------------------


#endif // uiStyleItemModel_h

