/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	uiStyleView.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 December 23
//-----------------------------------------------------------------------------


// Qanava headers
#include "./uiStyleView.h"
#include "../qanStyle.h"


// QT headers
#include <QToolBar>
#include <QHeaderView>
#include <QColorDialog>
#include <QPushButton>
#include <QToolButton>
#include <QHBoxLayout>
#include <QPainter>
#include <QItemEditorFactory>
#include <QFileDialog>
#include <QToolBar>


namespace qan { // ::qan


ColorEditWidget::ColorEditWidget( QWidget* parent, QColor color ) :
	QWidget( parent ),
	_label( 0 ),
	_color( color )
{
	setAutoFillBackground( true );   // Delegate widget are transparent by default

	QHBoxLayout* hbox = new QHBoxLayout( this );
	hbox->setMargin( 0 );
	hbox->setSpacing( 1 );

	_label = new QLabel( this );
	_label->setText( _color.name( ) );

	QPushButton* b = new QPushButton( "...", parent );
	b->setText( "..." );
	b->setMaximumWidth( 40 );
	connect( b, SIGNAL( clicked() ), this, SLOT( selectColor() ) );

	hbox->addSpacing( 2 );
	hbox->addWidget( _label );
	hbox->addWidget( b );

	setColor( color );
}

void	ColorEditWidget::setColor( QColor color )
{
	_color = color;

	QPalette palette;
	palette.setColor( backgroundRole( ), _color );
	setPalette( palette );

	QColor colorHsv = color.toHsv( );
	QColor textColor( colorHsv.saturation( ) < 127 ? Qt::black : Qt::white );
	palette = _label->palette( );
	palette.setColor( QPalette::Text, textColor );
	_label->setPalette( palette );
	_label->setText( _color.name( ) );
}

void	ColorEditWidget::selectColor( )
{
	QColor color = QColorDialog::getColor( _color, this );
	setColor( color );
	close( );
	/*if ( _delegate != 0 )
		emit _delegate->commitData( this );*/
}


StringEditWidget::StringEditWidget( QWidget* parent, QString s ) :
	QWidget( parent ),
	_edit( 0 ),
	_string( s )
{
	setAutoFillBackground( true );   // Delegate widget are transparent by default

	QHBoxLayout* hbox = new QHBoxLayout( this );
	hbox->setMargin( 0 );
	hbox->setSpacing( 1 );

	_edit = new QLineEdit( this );
	_edit->setFrame( false );

	QPushButton* b = new QPushButton( "...", parent );
	b->setText( "..." );
	b->setMaximumWidth( 40 );
	connect( b, SIGNAL( clicked() ), this, SLOT( selectString() ) );

	hbox->addSpacing( 2 );
	hbox->addWidget( _edit );
	hbox->addWidget( b );
	connect( _edit, SIGNAL( editingFinished() ), this, SLOT( editingFinished() ) );
	setString( s );
}

void	StringEditWidget::setString( QString s )
{
	_edit->setText( s );
	_edit->selectAll( );
	_string = s;
}

void	StringEditWidget::selectString( )
{
	QString s = QFileDialog::getOpenFileName( this, "Choose an image file", "./", "Images (*.png *.jpg)" );
	if ( !s.isEmpty( ) )
		setString( s );
}

void	StringEditWidget::editingFinished( )
{
	setString( _edit->text( ) );
}


StyleDelegate::StyleDelegate( QAbstractItemModel& model ) :
	_model( model )
{
	setItemEditorFactory( new QItemEditorFactory( ) );
}

QWidget*	StyleDelegate::createEditor ( QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const
{
	QWidget* editor = 0;

	QPalette palette( parent->palette( ) );

	QVariant attribute = _model.data( index );
	switch ( attribute.type( ) )
	{
	case QVariant::String:
		{
			QString s = attribute.toString( );
			editor = new StringEditWidget( parent, s );
			editor->setPalette( palette );
		}
		break;
	case QVariant::Color:
		{
			QColor color = attribute.value< QColor >( );
			editor = new ColorEditWidget( parent, color );
		}
		break;
	default:
		editor = itemEditorFactory( )->createEditor( attribute.type( ), parent );
		break;
	};

	if ( editor != 0 )
		editor->installEventFilter( const_cast< StyleDelegate* >( this ) );
	else
		return QItemDelegate::createEditor( parent, option, index );

	return editor;
}

void		StyleDelegate::paint( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	if ( !index.isValid( ) || index.row( ) == 0 || index.column( ) != 1 )
	{
		QItemDelegate::paint( painter, option, index );
		return;
	}

	QVariant attribute = _model.data( index );
	switch ( attribute.type( ) )
	{
	case QVariant::Color:
		{
			QColor color = attribute.value< QColor >( );

			painter->save( );				// StyleOption option does not work for back color
			painter->setPen( Qt::NoPen );
			painter->setBrush( color );
			painter->drawRect( option.rect );
			painter->restore( );

			// Invert text color to avoid text beeing unreadable
			QColor colorHsv = color.toHsv( );
			QColor textColor( colorHsv.saturation( ) < 127 ? Qt::black : Qt::white );
			QStyleOptionViewItem customOption( option );
			customOption.palette.setColor( QPalette::Text, textColor );
			drawDisplay( painter, customOption, option.rect, color.name( ) );
			break;
		}
	default:
		QItemDelegate::paint( painter, option, index );
		break;
	};
}

void		StyleDelegate::setEditorData( QWidget* editor, const QModelIndex& index ) const
{
	QVariant attribute = _model.data( index );

	switch ( attribute.type( ) )
	{
	case QVariant::String:
		{
			StringEditWidget* w = static_cast< StringEditWidget* >( editor );
			QVariant attribute = _model.data( index );
			w->setString( attribute.toString( ) );
		}
		break;
	case QVariant::Color:
		{
			ColorEditWidget* w = static_cast< ColorEditWidget* >( editor );
			QVariant attribute = _model.data( index );
			w->setColor( attribute.value< QColor >( ) );
		}
		break;
	default:
		QItemDelegate::setEditorData( editor, index );
		break;
	};
}

void		StyleDelegate::setModelData( QWidget* editor, QAbstractItemModel* model, const QModelIndex& index ) const
{
	QVariant attribute = model->data( index );

	switch ( attribute.type( ) )
	{
	case QVariant::String:
		{
			StringEditWidget* w = static_cast< StringEditWidget* >( editor );
			model->setData( index, w->getString( ) );
		}
		break;
	case QVariant::Color:
		{
			ColorEditWidget* w = static_cast< ColorEditWidget* >( editor );
			model->setData( index, w->getColor( ) );
		}
		break;
	default:
		QItemDelegate::setModelData( editor, model, index );
		break;
	};
}

QSize		StyleDelegate::sizeHint ( const QStyleOptionViewItem& option, const QModelIndex& index ) const
{
	return QSize( 50, 15 );
}



//-----------------------------------------------------------------------------
StyleEditor::StyleEditor( QWidget* parent, Style* style ) :
	QMainWindow( parent ),
	_style( style ),
	_tableView( 0 ),
	_cbType( 0 )
{
	if ( layout( ) != 0 )
	{
		layout( )->setMargin( 0 );
		layout( )->setSpacing( 1 );
	}
	QToolBar* toolBar = addToolBar( "Tools" );
	if ( toolBar->layout( ) != 0 )
		toolBar->layout( )->setMargin( 2 );
	toolBar->setIconSize( QSize( 16, 16 ) );

	_cbType = new QComboBox( toolBar );
	_cbType->addItem( QIcon(), "<< Type >>" );
	_cbType->addItem( QIcon(), "Color" );
	_cbType->addItem( QIcon(), "String" );
	_cbType->addItem( QIcon(), "Image" );
	_cbType->addItem( QIcon(), "Int" );
	_cbType->addItem( QIcon(), "Double" );

	QToolButton* addAttribute = new QToolButton( toolBar );
	addAttribute->setIcon( QIcon( "images/qanava_plus.png" ) );
	addAttribute->setAutoRaise( true );
	connect( addAttribute, SIGNAL( clicked() ), this, SLOT( addAttribute() ) );

	QToolButton* removeAttribute = new QToolButton( parent );
	removeAttribute->setIcon( QIcon( "images/qanava_minus.png" ) );
	removeAttribute->setAutoRaise( true );
	connect( removeAttribute, SIGNAL( clicked() ), this, SLOT( removeAttribute() ) );

	toolBar->addWidget( _cbType );
	//toolBar->addSeparator( );
	toolBar->addWidget( addAttribute );
	toolBar->addWidget( removeAttribute );

	_tableView = new QTableView( this );
	_tableView->setAlternatingRowColors( true );
	_tableView->setMaximumWidth( 200 );
	_tableView->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	_tableView->horizontalHeader( )->resizeSections( QHeaderView::Fixed );  // Fixed
	_tableView->horizontalHeader( )->setStretchLastSection( true );
	_tableView->verticalHeader( )->resizeSections( QHeaderView::Fixed );
	_tableView->verticalHeader( )->setDefaultSectionSize( 20 );
	_tableView->verticalHeader( )->hide( );

	setCentralWidget( _tableView );
	setStyle( style );
}

void	StyleEditor::setStyle( Style* style )
{
	if ( style != 0 )
	{
		_style = style;
		_tableView->setDisabled( false );
		_tableView->setItemDelegate( new StyleDelegate( *style ) );
		_tableView->setModel( style );
	}
	else
	{
		_style = 0;
		_tableView->reset( );
		_tableView->setDisabled( true );
	}
}

void	StyleEditor::addAttribute( )
{
	QString attrName( "New attribute " );
	attrName += QString::number( _style->size( ) );
	switch ( _cbType->currentIndex( ) )
	{
	case 1:  // Color
		_style->add( attrName, Qt::black );
		break;
	case 2:  // String
		_style->add( attrName, QString( "" ) );
		break;
	case 3:  // Image
		break;
	case 4:  // Int
		break;
	case 5:  // Double
		break;
	default:
		break;
	}
}

void	StyleEditor::removeAttribute( )
{

}
//-----------------------------------------------------------------------------


} // ::qan

