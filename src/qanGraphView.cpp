/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canGraphView.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2006 July 29
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanGraphView.h"
#include "./qanGridItem.h"


// QT headers
#include <QPen>
#include <QMouseEvent>


//-----------------------------------------------------------------------------
namespace qan { // ::qan


/* GraphView Constructors/Destructors *///----------------------------------
GraphView::GraphView( QWidget* parent, QColor backColor, QSize size ) :
	QGraphicsView( parent ),
	_graph( 0 ),
	_grid( 0 ),
	_controllerManager( this ),
	_zoom( 1.0 ),
	_zoomMaxFactor( 2 ),
	_layout( new Random( ) )
{
	configureView( backColor, size );

	getControllerManager( ).registerController( new PanController( *this ) );
	getControllerManager( ).registerController( new ZoomWindowController( *this ) );
	getControllerManager( ).registerController( new ZoomController( *this ) );
}

GraphView::GraphView( Graph& graph, QWidget* parent, QColor backColor, QSize size ) :
	QGraphicsView( &graph.getM( ), parent ),
	_graph( &graph ),
	_grid( 0 ),
	_controllerManager( this ),
	_zoom( 1.0 ),
	_zoomMaxFactor( 2 )
{
	configureView( backColor, size );

	getControllerManager( ).registerController( new PanController( *this ) );
	getControllerManager( ).registerController( new ZoomWindowController( *this ) );
	getControllerManager( ).registerController( new ZoomController( *this ) );

	setGraph( graph );
}

void	GraphView::configureView( QColor backColor, QSize size )
{
	setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
	setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
	setAlignment( Qt::AlignLeft | Qt::AlignTop );
	setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

	//connect( _graphicsView, SIGNAL(itemDoubleClicked(QGraphicsItem*)), this, SLOT(graphicItemDoubleClicked(QGraphicsItem*)) );

	QPalette palette;
	palette.setColor( backgroundRole( ), backColor );
	viewport( )->setPalette( palette );
}

void	GraphView::setGraph( Graph& graph )
{
	_graph = &graph;
	GraphScene& graphScene = graph.getM( );
	graphScene.setItemIndexMethod( QGraphicsScene::NoIndex );
	graphScene.registerGraphicNodeFactory( new NodeItem::DefaultFactory( ) );
	graphScene.init( graph.getRootNodes( ) );
	setScene( &graphScene );
}
//-----------------------------------------------------------------------------



/* Grid Management *///--------------------------------------------------------
void	GraphView::drawBackground( QPainter* painter, const QRectF& rect )
{
	if ( _grid != 0 )
		_grid->drawBackground( *painter, rect );
}
//-----------------------------------------------------------------------------



/* View Controller Management *///---------------------------------------------
void	GraphView::keyPressEvent( QKeyEvent* e )
{
	QGraphicsView::keyPressEvent( e );

	if ( _controllerManager.keyPressEvent( e ) )
		viewport( )->update( );
}

void	GraphView::mousePressEvent( QMouseEvent* e )
{
	QGraphicsView::mousePressEvent( e );

	if ( _controllerManager.mousePressEvent( e ) )
		viewport( )->update( );
}

void	GraphView::mouseReleaseEvent( QMouseEvent* e )
{
	QGraphicsView::mouseReleaseEvent( e );

	if ( _controllerManager.mouseReleaseEvent( e ) )
		viewport( )->update( );
}

void	GraphView::mouseMoveEvent( QMouseEvent* e )
{
	QGraphicsView::mouseMoveEvent( e );

	if ( _controllerManager.mouseMoveEvent( e ) )
		viewport( )->update( );
}

void	GraphView::mouseDoubleClickEvent( QMouseEvent* e )
{
	QGraphicsView::mouseDoubleClickEvent( e );

	if ( _controllerManager.mouseDoubleClickEvent( e ) )
		viewport( )->update( );

	QGraphicsItem* item = itemAt( e->pos( ) );
	if ( item != 0 )
		emit itemDoubleClicked( item->parentItem( ) != 0 ? item->parentItem( ) : item );
}

void	GraphView::wheelEvent( QWheelEvent* e )
{
	QGraphicsView::wheelEvent( e );

	if ( _controllerManager.wheelEvent( e ) )
		viewport( )->update( );
}

QAction*	GraphView::getAction( QString name )
{
	Controller* controller = _controllerManager.getController( name );
	if ( controller != 0 )
		return controller->getAction( );

	Controller::Manager::iterator controllerIter = _controllerManager.begin( );
	for ( ; controllerIter != _controllerManager.end( ); controllerIter++ )
	{
		QAction* action = ( *controllerIter )->getAction( name );  // Create an action by name
		if ( action != 0 )
			return action;
	}
	return 0;
}
//-----------------------------------------------------------------------------



/* Zoom Management  *///-------------------------------------------------------
void	GraphView::setZoom( double zoom )
{
	qreal factor = matrix( ).scale( zoom, zoom ).mapRect( QRectF( 0., 0., 1., 1.) ).width( );
	if ( factor < 0.07 || factor > 100 )
		return;

	if ( zoom > 0.1 )
	{
		_zoom = zoom;
		QMatrix m;
		m.scale( zoom, zoom );
		setMatrix( m );
	}
}

/*!
	With a maximum zoom factor of 2 (this default value can be changed with setZoomMaxFactor() method), a
	normal zoom (1) is obtained by setting the zoom to 50% (half of the maximum magnification
	factor).

	\param	value Zoom value in percent between (almost) 0 and the max zoom magnification (must be 0_1 to 100).
 */
void	GraphView::setZoomPercent( int value )
{
	double zoomPercent = value;
	if ( zoomPercent == 0 )
		zoomPercent = 1;
	if ( zoomPercent > 100 )
		zoomPercent = 100;

	// Set zoom factor
	double zoomMaxFactor = ( _zoomMaxFactor < 0.1 ? 0.1 : _zoomMaxFactor );
	setZoom( zoomPercent / 100.f * zoomMaxFactor );
}
//-----------------------------------------------------------------------------



/* Style Management *///-------------------------------------------------------
/*! This is a shortcut method, style's should usually be managed trough GraphScene's
	methods.
 */
void	GraphView::applyStyle( Node* node, Style* style )
{
	if ( getGraphScene( ) != 0 )
		getGraphScene( )->applyStyle( node, style );
}
//-----------------------------------------------------------------------------



/* Layout Management *///------------------------------------------------------
/*!
	\note The ownership of the given layout object is transferred to this view.
	\param layout	New layout to use with this view (must not be 0).
 */
void	GraphView::setGraphLayout( Layout* layout, QProgressDialog* progress )
{
	if ( layout == 0 )
		return;

	if ( _layout != 0 )
	{
		//delete _layout;
		_layout = 0;
	}
	_layout = layout;
}

void	GraphView::layoutGraph( QProgressDialog* progress, Layout* layout, int step, Node* except )
{
	if ( _graph == 0 )
		return;
	if ( _layout == 0 && layout == 0 )
		return;

	QRectF r = sceneRect( );
	r.setWidth( std::max( r.width( ), ( double )width( ) ) );
	r.setHeight( std::max( r.height( ), ( double )height( ) ) );

	assert( getGraphScene( ) != 0 );
	if ( layout != 0 )
		layout->layout( *_graph, getGraphScene( ), r, progress, step );
	else if ( _layout != 0 && getGrid( ) != 0 )
		_layout->layout( *_graph, getGraphScene( ), r, progress, step );

	getGraphScene( )->updatePositions( );

	update( );
}
//-----------------------------------------------------------------------------


} // ::qan
//-----------------------------------------------------------------------------


