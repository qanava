/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canGraphView.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2006 July 29
//-----------------------------------------------------------------------------


#ifndef canGraphView_h
#define canGraphView_h


// Qanava headers
#include "./qanController.h"
#include "./qanLayout.h"
#include "./qanGraphScene.h"
#include "./qanGraph.h"


// QT headers
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QProgressDialog>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	class GridItem;
	class Grid;
	class Layout;

	class GraphView : public QGraphicsView
	{
		Q_OBJECT

		/*! \name GraphView Constructors/Destructors *///-------------------
		//@{
	public:

		GraphView( QWidget* parent = 0, QColor backColor = QColor( 170, 171, 205 ), QSize size = QSize( 300, 150 ) );

		GraphView( Graph& graph, QWidget* parent = 0,
				   QColor backColor = QColor( 170, 171, 205 ), QSize size = QSize( 300, 150 ) );

		virtual QSize	sizeHint( ) const { return QSize( 300, 250 ); }

		void			setGraph( Graph& graph );

	protected:

		GraphScene*		getGraphScene( ) { return &_graph->getM( ); }

		void			configureView( QColor backColor, QSize size );

	private:

		Graph*			_graph;
		//@}
		//---------------------------------------------------------------------



		/*! \name Grid Management *///-----------------------------------------
		//@{
	public:

		void			setGrid( GridItem* grid ) { _grid = grid; }

		GridItem*		getGrid( ) { return _grid; }

	protected:

		virtual void	drawBackground( QPainter* painter, const QRectF& rect );

	private:

		GridItem*		_grid;
		//@}
		//---------------------------------------------------------------------



		/*! \name View Controllers Management *///-----------------------------
		//@{
	protected:

		virtual void		keyPressEvent( QKeyEvent* e );

		virtual void		mousePressEvent( QMouseEvent* e );

		virtual void		mouseReleaseEvent( QMouseEvent* e );

		virtual void		mouseMoveEvent( QMouseEvent* e );

		virtual	void		mouseDoubleClickEvent( QMouseEvent* e );

		virtual	void		wheelEvent( QWheelEvent* e );

	signals:

		void				itemDoubleClicked( QGraphicsItem* item );

	public:

		//! Get this item view controller manager.
		Controller::Manager&	getControllerManager( ) { return 	_controllerManager; }

		//! Get the action for a controller with a given name.
		QAction*				getAction( QString name );

	private:

		Controller::Manager		_controllerManager;
		//@}
		//---------------------------------------------------------------------



		/*! \name Zoom Management  *///----------------------------------------
		//@{
	public:

		//! Set the maximum zoom factor that can be sets with the setZoomValue() method.
		void			setZoomMaxFactor( double zoomMaxFactor ) { _zoomMaxFactor = zoomMaxFactor; }

		double			getZoom( ) const { return _zoom; }

		void			setZoom( double zoom );

	public slots:

		//! Set the current zoom factor value in percents (from 1 to 100, 100 sets the maximum zoom factor).
		void			setZoomPercent( int value );

	private:

		double			_zoom;

		//! Maximum zoom factor (default = 2).
		double			_zoomMaxFactor;
		//@}
		//---------------------------------------------------------------------



		/*! \name Style Management *///----------------------------------------
		//@{
	public:

		//! Apply a style on a specific node (shortcut to GraphMode::applyStyle()).
		void	applyStyle( Node* node, Style* style );
		//@}
		//---------------------------------------------------------------------



		/*! \name Layout Management *///---------------------------------------
		//@{
	public:

		//! .
		void			setGraphLayout( qan::Layout* layout, QProgressDialog* progress = 0 );

		//! .
		Layout*			getGraphLayout( Layout& layout ) { return _layout; }

		//! .
		const Layout*	getGraphLayout( Layout& layout ) const { return _layout; }

		//! .
		void			layoutGraph( QProgressDialog* progress = 0, Layout* layout = 0, int step = -1, Node* except = 0 );

	protected:

		//! .
		Layout*			_layout;
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // canGraphView_h

