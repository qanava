/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laTimeTree.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 December 05
//-----------------------------------------------------------------------------


#ifndef laTimeTree_h
#define laTimeTree_h


// Qanava headers
#include "./qanLayout.h"
#include "./qanGrid.h"


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	//! Layout a graph in space taking into account group membership (clusters) and respecting a chronological order.
	/*!

		<img src="./images/qan-timetree_shot.png" alt="Qanava timetree sample">

		\nosubgrouping
	*/
	class TimeTree : public Layout
	{
	public:

		//! Sort nodes on the basis of their date (older first).
		struct NodeDateComp
		{
			bool operator()( const Node* n1, const Node* n2 ) const;
		};

		//! STL set sorting node by date.
		typedef std::multiset< Node*, NodeDateComp >	NodeSet;

		//! Manager of NodeSet objects.
		typedef std::list< NodeSet* >					NodeSetManager;

	protected:

		//! Construct the grid for a cluster based time tree graph layout.
		/*!
			\nosubgrouping
		*/
		class GridBuilder
		{
			/*! \name TimeTree Grid Management *///----------------------------
			//@{
		public:

			//! .
			GridBuilder( Grid& grid ) : _grid( grid ) { }

		public:

			//! Add _one_ grid element for a given cluster coordinates.
			void	buildClusterGrid( int cy, int w, int h, int r, int g, int b );

			//! Add all horizontal time grid elements for the given (sorted) nodes.
			void	buildTimeGrid( NodeSet& nodes, int width, int height );

			//! .
			void	buildTimeLabel( Node& node, int x, int y );

			//! .
			Grid&	getGrid( ) { return _grid; }

		private:

			//! .
			Grid&	_grid;
			//@}
			//-----------------------------------------------------------------
		};

	public:

		//! Models a group of nodes in the same track (usually a group equals a cluster of nodes).
		/*!
			\nosubgrouping
		*/
		class Group
		{
		private:

			class GroupMedDateComp
			{
			public:
				bool operator()( const Group* g1, const Group* g2 ) const;
			};

		public:

			//! .
			Group( ) { }

			//! .
			~Group( ) { }

			//! Add a group of nodes corresponding to a line of nodes for a given cluster.
			/*! The node must be sorted by date ascending order.							*/
			void				addLine( Node::List* lineNodes ) { _lines.push_back( lineNodes ); }

			//! Get the number of lines in this group.
			int					getLineCount( ) const { return _lines.size( ); }

			//! Get the ith line in this group.
			Node::List*		getLine( unsigned int line );

			//! Collect this group nodes to a given node list (list can be non emtpy).
			void				collectNodes( Node::List& nodes ) const;

			//! Affect a given style type to all of this group nodes.
			void				setNodesType( int type );

			//! Test if a group intersect with this group time interval.
			bool				intersect( const Group& group ) const;

			//! Get this group median date.
			QDateTime			getMedianDate( ) const;

			typedef std::list< Group* >							Groups;

			typedef std::vector< Node::List* >				Lines;

			//! STL multi set with median date group sorting.
			typedef std::multiset< Group*, GroupMedDateComp >	GroupSet;

		protected:

			//! Nodes in each lines must be stored in .
			Lines		_lines;
		};


	protected:

		//! Models a line of related groups of nodes (the track form a line, but the group in the track can take ultiple single lines).
		/*!
			\nosubgrouping
		*/
		class Track
		{
		public:

			Track( ) { }

			void	addGroup( Group* group ) { _groups.push_back( group ); }

			//! Test if a given track time duration interval is disjoint from this track own duration.
			bool	isDisjointOf( Track& track );

			//! Merge a given track groups to this track.
			void	mergeTrack( Track& track );

			typedef std::list< Track* >	Tracks;

			int		getLineCount( ) const;

			float	layout( float trackY, float width, GridBuilder& gridBuilder );

			//! Collect groups in this track sorted by group's average date.
			void	collectGroups( Group::Groups& groups );

			Group::Groups&	getGroups( ) { return _groups; }

			int		getNodeCount( ) const;

		protected:

		private:

			Group::Groups	_groups;
		};


		//! Sort tracks on the basis of their node (spatial) density.
		struct TrackDensityComp
		{
			bool operator()( const Track* t1, const Track* t2 ) const;
		};


		//! Model a series of tracks.
		/*!
			\nosubgrouping
		*/
		class Tracks
		{
		public:

			Tracks( ) { }

			void	addTrack( Track* track );

			void	mergeTracks( );

			float	layout( float width, GridBuilder& gridBuilder );

			//! Collect all tracks group to a group list in the left-right top to bottom order.
			void	collectGroups( Group::Groups& groups );

			void	sortByNodeDensity( );

		protected:

			Track::Tracks	_tracks;
		};


		/*! \name TimeTree Constructor/Destructor *///-------------------------
		//@{
	public:

		//! TimeTree constructor.
		TimeTree( NodeSetManager& nodeSetManager ) :
			Layout( ),
			_nodeSetManager( nodeSetManager ) { }

		~TimeTree( )
		{

		}

	private:

		//! .
		NodeSetManager&		_nodeSetManager;
		//@}
		//---------------------------------------------------------------------



		/*! \name TimeTree Layout Generation Management *///-------------------
		//@{
	public:

		static float incX;

		static float lineIntervalY;

		static float trackIntervalY;

		//! .
		virtual void	layout( Graph& graph, Grid& grid, int width, int height, QProgressDialog* progress = 0 );

	protected:

		//! Set nodes horizontal position, and return the resulting layout width.
		float			layoutNodesX( NodeSet& nodes );

		//! .
		Group*			buildGroup( NodeSet& clusterNodes );
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // laTimeTree_h

