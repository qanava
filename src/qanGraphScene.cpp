/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canGraphScene.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 22
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanGraph.h"
#include "./qanEdge.h"
#include "./qanGraphScene.h"
#include "./qanGrid.h"


// STD headers
#include <cmath>


// QT headers
#include <QVBoxLayout>
#include <QTimer>
#include <QScrollBar>


//-----------------------------------------------------------------------------
namespace qan { // ::qan


/* GraphScene Object Management *///-------------------------------------------
/*!
	\param	backgroundColor	Graph canvas background color.
 */
GraphScene::GraphScene( QWidget* parent, QColor backgroundColor, QSize size ) :
	QGraphicsScene( parent )
{

}

GraphScene::~GraphScene( )
{
	_styleManager.clear( );
}
//-----------------------------------------------------------------------------



/* Scene Management *///-------------------------------------------------------
/*! Styles registered in the style manager are not cleared, and can be reused when this
    model will receive new edges and noeds.
 */
void	GraphScene::clear( )
{
    // Clear all graphics items in the graphic view
	foreach ( QGraphicsItem* item, _edgeGraphicItemMap.values( ) )
		removeItem( item );
	foreach ( AbstractNodeItem* item, _nodeGraphicItemMap.values( ) )
		removeItem( item->getGraphicsItem( ) );

	// Clear all mappings
	_nodeGraphicItemMap.clear( );
	_edgeGraphicItemMap.clear( );
}

VectorF	GraphScene::getBoundingBox( const Node::List& nodes )
{
	VectorF bbox( 2 );
	bbox( 0 ) = 0;
	bbox( 1 ) = 0;

	Node::List::const_iterator nodeIter = nodes.begin( );
	for ( ; nodeIter != nodes.end( ); nodeIter++ )
	{
		const Node* node = *nodeIter;
		const VectorF& position = node->getPosition( );
		const VectorF& dimension = node->getDimension( );

		if (  position( 0 ) + dimension( 0 ) > bbox( 0 ) )
			bbox( 0 ) = position( 0 ) + dimension( 0 );
		if (  position( 1 ) + dimension( 1 )  > bbox( 1 ) )
			bbox( 1 ) = position( 1 ) + dimension( 1 );
	}
	return bbox;
}

void	GraphScene::updatePositions( Node* except )
{
	// Update all nodes positions
	NodeGraphicItemMap::iterator nodeIter = _nodeGraphicItemMap.begin( );
	for ( ; nodeIter != _nodeGraphicItemMap.end( ); nodeIter++ )
	{
		const Node* node = nodeIter.key( );
		AbstractNodeItem* item = nodeIter.value( );
		if ( node != 0 && item != 0 && node != except )
		{
			item->getGraphicsItem( )->setPos( node->getPosition( )( 0 ), node->getPosition( )( 1 ) );
			item->updateEdges( );
		}
	}
}
//-----------------------------------------------------------------------------



/* Style Management *///-------------------------------------------------------
void	GraphScene::applyStyle( void* item, Style* style )
{
	if ( item != 0 && style != 0 )
	{
		_styleManager.setStyle( item, *style );
		updateStyle( item, style );
	}
}

void	GraphScene::updateStyle( void* item, Style* style )
{
	// Update node graphic item with the style content
	NodeGraphicItemMap::const_iterator nodeIter = _nodeGraphicItemMap.find( reinterpret_cast< Node* >( item ) );
	if ( nodeIter != _nodeGraphicItemMap.end( ) )
	{
		AbstractNodeItem* item = nodeIter.value( );
		item->setStyle( style );
	}
	EdgeGraphicItemMap::const_iterator edgeIter = _edgeGraphicItemMap.find( reinterpret_cast< Edge* >( item ) );
	if ( edgeIter != _edgeGraphicItemMap.end( ) )
	{
		GraphItem* item = edgeIter.value( );
		item->setStyle( style );
	}
}
//-----------------------------------------------------------------------------



/* Scene Topology Management *///----------------------------------------------
void	GraphScene::init( Node::List& rootNodes )
{
	// Generate persistent model indexes for all graph nodes
	Node::List::iterator nodeIter = rootNodes.begin( );
	for ( int row = 0; nodeIter != rootNodes.end( ); row++, nodeIter++ )
		visit( **nodeIter );
}

void	GraphScene::edgeInserted( qan::Edge& edge )
{
	insertEdge( edge );
}

void	GraphScene::edgeRemoved( qan::Edge& edge )
{

}

void	GraphScene::nodeInserted( qan::Node& node )
{
	insertNode( node );
}

void	GraphScene::nodeRemoved( qan::Node& node )
{
	QGraphicsItem* nodeItem = getNodeGraphicItem( &node );

	// Remove arrows
	Edge::List edges;
	std::copy( node.getInEdges( ).begin( ), node.getInEdges( ).end( ), std::back_insert_iterator< Edge::List >( edges ) );
	std::copy( node.getOutEdges( ).begin( ), node.getOutEdges( ).end( ), std::back_insert_iterator< Edge::List >( edges ) );
	for ( Edge::List::iterator edgeIter = edges.begin( ); edgeIter != edges.end( ); edgeIter++ )
	{
		EdgeGraphicItemMap::iterator canvasEdgeIter = _edgeGraphicItemMap.find( *edgeIter );
		if ( canvasEdgeIter != _edgeGraphicItemMap.end( ) )
		{
			( canvasEdgeIter.value( ) )->disconnectEdge( );
			_edgeGraphicItemMap.remove( *edgeIter );
		}
	}

	_nodeGraphicItemMap.remove( &node );
	delete nodeItem;
	update( );
}

void	GraphScene::nodeChanged( qan::Node& node )
{
	NodeItem* nodeItem = static_cast< NodeItem* >( getNodeGraphicItem( &node ) );
	if (  nodeItem != 0 )
		nodeItem->updateItem( );
}

void	GraphScene::visit( qan::Node& node )
{
	if ( _nodeGraphicItemMap.find( &node ) != _nodeGraphicItemMap.end( ) )
		return;
	insertNode( node );

	// Visit sub nodes
	Edge::List& edgeList = node.getOutEdges( );
	for ( Edge::List::iterator edgeIter = edgeList.begin( ); edgeIter != edgeList.end( ); edgeIter++ )
	{
		Edge* edge = *edgeIter;
		visit( edge->getDst( ) );
	}
}

void	GraphScene::insertNode( qan::Node& node )
{
	// Map a graphic item to a graph node
	AbstractNodeItem* nodeItem = createNodeItem( node, 0, QPoint( 1, 1 ), node.getLabel( ) );
	if ( nodeItem != 0 )
		_nodeGraphicItemMap.insert( &node, nodeItem );

	// Generate arrows for the model
	Edge::Set edges = Edge::Set::fromList( node.getInEdges( ) );
	edges.unite( Edge::Set::fromList( node.getOutEdges( ) ) );
	foreach ( Edge* edge, edges )
		insertEdge( *edge );
}

void	GraphScene::insertEdge( qan::Edge& edge )
{
	EdgeGraphicItemMap::iterator canvasEdgeIter = _edgeGraphicItemMap.find( &edge );
	if ( canvasEdgeIter == _edgeGraphicItemMap.end( ) )
	{
		NodeItem* src = static_cast< NodeItem* >( getNodeGraphicItem( &edge.getSrc( ) ) );
		NodeItem* dst = static_cast< NodeItem* >( getNodeGraphicItem( &edge.getDst( ) ) );
		if ( src != 0 && dst != 0 )
		{
			// Get node type associed style
			Style* style = getStyleManager( ).getEmptyStyle( );
			Style* edgeStyle = getStyleManager( ).getStyle( &edge );
			if ( edgeStyle != 0 )
				style = edgeStyle;

			EdgeItem* edgeItem = new EdgeItem( getStyleManager( ), style, 0, this, edge, src, dst );
			_edgeGraphicItemMap.insert( &edge, edgeItem );
		}
	}
	else
		canvasEdgeIter.value( )->show( );   // The arrow might have been previously hiden, so set it visible again
}
//-----------------------------------------------------------------------------



/* Model Index and Graphic Item Management *///--------------------------------
Node*		GraphScene::getGraphicItemNode( const QGraphicsItem* item )
{
	return const_cast< Node* >( _nodeGraphicItemMap.key( reinterpret_cast< AbstractNodeItem* >( const_cast< QGraphicsItem* >( item ) ) ) );
}

QGraphicsItem*	GraphScene::getNodeGraphicItem( const Node* node )
{
	if ( node == 0 )
		return 0;
	NodeGraphicItemMap::const_iterator nodeIter = _nodeGraphicItemMap.find( node );
	if ( nodeIter != _nodeGraphicItemMap.end( ) )
		return ( nodeIter.value( ) )->getGraphicsItem( );
	return 0;
}

Node*	GraphScene::getCurrentNode( QPoint& p )
{
	/*Item::List collisions;
	//getCanvas( )->getCollisions( p, collisions );  // FIXME
	if ( collisions.begin( ) != collisions.end( ) )
	{
		Item* item = *collisions.begin( );
		//if ( getCanvas( ) != 0 && !getCanvas( )->isFreed( item ) )  // FIXME
		//	return getGraphicItemNode( item );
	}*/
	return 0;
}
//-----------------------------------------------------------------------------


/* Graphic Node Factory Management *///----------------------------------------
/*!	Ownership for the factory is transfrerred to this graph item view.
	\param	factory	Graphic node factory that must be used when generating node graphic counterpart.
 */
void	GraphScene::registerGraphicNodeFactory( AbstractNodeItem::AbstractFactory* factory )
{
	assert( factory != 0 );
	if ( factory->isDefaultFactory( ) )
		_factories.push_back( factory );	// Default factories must be tested after standard ones
	else
		_factories.push_front( factory );
}

/*!
	\return	A adequat graphic counterpart for node, or a node from a default factory, or 0 if no (default) factories are registered.
 */
AbstractNodeItem*	GraphScene::createNodeItem( Node& node, QGraphicsItem* parent,
												   QPoint origin, const QString& label )
{
	// Get node type associed style
	Style* style = getStyleManager( ).getEmptyStyle( );
	Style* nodeStyle = getStyleManager( ).getStyle( &node );
	if ( nodeStyle != 0 )
		style = nodeStyle;
	nodeStyle = getStyleManager( ).getStyle( node.getType( ) );
	if ( nodeStyle != 0 )
		style = nodeStyle;

	// Find a factory able to create a grpahic item for my node
	AbstractNodeItem::AbstractFactory::List::iterator factoryIter = _factories.begin( );
	for ( ; factoryIter != _factories.end( ); factoryIter++ )
	{
		AbstractNodeItem::AbstractFactory* factory = *factoryIter;
		if ( factory->isDefaultFactory( ) )	// Default factories create graphic node whatever the node type is
			return factory->create( node, getStyleManager( ), parent, this, style, origin, label );

		if ( node.getType( ) == factory->getNodeType( ) )
			return factory->create( node, getStyleManager( ), parent, this, style, origin, label );
	}
	return 0;  // No adequat nor default factory found
}
//-----------------------------------------------------------------------------


} // ::qan
//-----------------------------------------------------------------------------


