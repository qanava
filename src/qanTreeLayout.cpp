/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanTreeLayout.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 January 06
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanTreeLayout.h"


namespace qan { // ::qan


/* Hierarchy Layout Generation Management *///---------------------------------
void	HierarchyTree::layout( Graph& graph, QGraphicsScene*, QRectF r, QProgressDialog* progress, int step )
{
	// Configure the progress monitor
	if ( progress != 0 )
	{
		progress->setMaximum( graph.getNodeCount( ) );
		progress->setValue( 0 );
	}

	// Layout all graph subgraph
	_marked.clear( );
	QRectF bbox( _origin, QSizeF( 0., 0. ) );
	foreach ( Node* root, graph.getRootNodes( ) )
	{
		layout( *root, bbox, 0, progress );
		bbox.translate( _spacing.x( ), 0. );
	}

	// Transpose nodes position coordinates if the layout is horizontal
	if ( _orientation == Qt::Horizontal )
	{
		foreach( Node* node, graph.getNodes( ) )
		{
			VectorF position = node->getPosition( );
			node->setPosition( position( 1 ), position( 0 ) );
		}
	}

	if ( progress != 0 )
		progress->close( );
}

void	HierarchyTree::layout( Node& node, QRectF& bbox, int depth, QProgressDialog* progress, int step )
{
	if ( _marked.contains( &node ) )
		return;
	_marked.insert( &node );

	if ( progress != 0 && progress->wasCanceled( ) )
		return;

	double xStart = bbox.left( );

	// Depth first
	int laidOut = 0;
	Node::Set outNodes; node.collectOutNodesSet( outNodes );
	foreach ( Node* subNode, outNodes )
	{
		if ( !_marked.contains( subNode ) )
		{
			if ( laidOut > 0 )
				bbox.translate( _spacing.x( ), 0. );

			layout( *subNode, bbox, depth + 1, progress );
			laidOut++;
		}
	}

	double xEnd = bbox.left( );

	// Detect leaf node
	double x = 0.f;
	double y = _origin.y( ) + ( _spacing.y( ) * depth );
	if ( node.isLeaf( ) )
		x = xStart;
	else
		x = xStart + ( ( xEnd - xStart ) / 2.f );
	node.setPosition( x, y );

	if ( progress != 0 )
		progress->setValue( progress->value( ) + 1 );
}
//-----------------------------------------------------------------------------


/* ChanTree Management *///----------------------------------------------------
ChanTree::ChanTree( ) : Layout( )
{

}

void	ChanTree::layout( Graph& graph, QGraphicsScene* scene, QRectF r, QProgressDialog* progress, int step )
{
	// Configure the progress monitor
	if ( progress != 0 )
	{
		progress->setMaximum( graph.getNodeCount( ) );
		progress->setValue( 0 );
	}

	// Layout all graph subgraph
	Node::Set leafTrees;

	QMap< Node*, QRectF > bboxes;
	foreach ( Node* root, graph.getRootNodes( ) )
	{
		findLeafTrees( *root, leafTrees );
		layoutSubTree( *root, leafTrees, bboxes );

		VectorF origin( 2 ); origin( 0 ) = 0.; origin( 1 ) = 0.;
		transformPositions( scene, *root, origin, bboxes, leafTrees );
	}

	if ( progress != 0 )
		progress->close( );
}

void	ChanTree::layoutSubTree( Node& node, Node::Set& leafTrees, QMap< Node*, QRectF >& bboxes )
{
	// If we are at a leaf tree (tree of depth 2), perform a trivial layout
	if ( leafTrees.contains( &node ) )
	{
		trivialLayout( node, bboxes );
		return;
	}

	// Get the subtrees drawn
	float x = 0.;
	Node::List outNodes; node.collectOutNodes( outNodes );
	foreach ( Node* subNode, outNodes )
		layoutSubTree( *subNode, leafTrees, bboxes );

	// Split the subnodes in two equal sets
	Node::List leftNodes;
	Node::List rightNodes;
	if ( outNodes.size( ) >= 2 )
	{
		int mid = outNodes.size( ) / 2;
		leftNodes = outNodes.mid( 0, mid );
		rightNodes = outNodes.mid( mid );
		assert( leftNodes.size( ) + rightNodes.size( ) == outNodes.size( ) );

		QRectF leftBox = computeHorizontalBbox( leftNodes, bboxes );
		QRectF rightBox = computeHorizontalBbox( rightNodes, bboxes );
		if ( leftBox.height( ) <= rightBox.height( ) )
			applyLeftRule( node, leftNodes, rightNodes, bboxes );
		else
			applyRightRule( node, leftNodes, rightNodes, bboxes );

		double width = leftBox.width( ) + rightBox.width( );
		double height = leftBox.height( ) + rightBox.height( );
		computeBBox( node, bboxes );	// Force bounding box computation and caching
	}
	else
	{
		// Only zero or one subnode
		double height = 0.;
		foreach ( Node* subNode, outNodes )
		{
			subNode->setPosition( 0., 50. );
			height = qMax( height, computeBBox( *subNode, bboxes ).height( ) );
		}
		computeBBox( node, bboxes );	// Force bounding box computation and caching
	}
}

void	ChanTree::applyLeftRule( Node& root, Node::List& left, Node::List& right, QMap< Node*, QRectF >& bboxes )
{
	// Align the left subtree on the left just under the root node
	QRectF leftBox = computeHorizontalBbox( left, bboxes );
	QRectF rightBox = computeHorizontalBbox( right, bboxes );
	double x = -rightBox.width( ) / 4;	// Do not strictly align bboxes vertically
	foreach ( Node* node, left )
	{
		QRectF leftNodeBox = computeBBox( *node, bboxes );
		x -= ( leftNodeBox.width( ) / 2. );	// Placing the node at the middle of its sub tree bbox
		node->setPosition( x, 25. );
		x -= ( leftNodeBox.width( ) / 2. );	// Adding the rest of the bbox and the space between subtrees
	}

	// Align the right subtree under the root and below the left block
	x = 0.;
	foreach ( Node* node, right )
	{
		QRectF rightNodeBox = computeBBox( *node, bboxes );
		x += ( rightNodeBox.width( ) / 2. );	// Placing the node at the middle of its sub tree bbox
		node->setPosition( x, leftBox.height( ) );
		x += ( rightNodeBox.width( ) / 2. );	// Placing the node at the middle of its sub tree bbox
	}
	foreach ( Node* node, right )
	{
		VectorF position = node->getPosition( );
		node->setPosition( position( 0 ) - x / 2., leftBox.height( ) );	// Convert to relative coordinates by removing mean
	}
}

void	ChanTree::applyRightRule( Node& root, Node::List& left, Node::List& right, QMap< Node*, QRectF >& bboxes )
{
	// Align the right subtree on the right just under the root node
	QRectF leftBox = computeHorizontalBbox( left, bboxes );
	QRectF rightBox = computeHorizontalBbox( right, bboxes );
	double x = leftBox.width( ) / 4;
	foreach ( Node* node, right )
	{
		QRectF rightNodeBox = computeBBox( *node, bboxes );
		x += ( rightNodeBox.width( ) / 2. );	// Placing the node at the middle of its sub tree bbox
		node->setPosition( x, 25. );
		x += ( rightNodeBox.width( ) / 2. );	// Adding the rest of the bbox and the space between subtrees
	}

	// Align the left subtree under the root and below the left block
	x = 0.;
	foreach ( Node* node, left )
	{
		QRectF leftNodeBox = computeBBox( *node, bboxes );
		x += ( leftNodeBox.width( ) / 2. );		// Placing the node at the middle of its sub tree bbox
		node->setPosition( x, rightBox.height( ) );
		x += ( leftNodeBox.width( ) / 2. );		// Adding the rest of the bbox and the space between subtrees
	}
	foreach ( Node* node, left )
	{
		VectorF position = node->getPosition( );
		node->setPosition( position( 0 ) - x / 2., rightBox.height( ) );	// Convert to relative coordinates by removing mean
	}
}

void	ChanTree::trivialLayout( Node& node, QMap< Node*, QRectF >& bboxes )
{
	// Layout sub nodes
	double x = 0.;
	double y = 50.;
	foreach ( Edge* edge, node.getOutEdges( ) )
	{
		edge->getDst( ).setPosition( x, y );
		x += 70.;
	}
	x -= 70.;	// Do not add space after the last node

	// Use relative coordinates centered on 0 (removing the mean)
	foreach ( Edge* edge, node.getOutEdges( ) )
	{
		Node& subNode = edge->getDst( );
		const qan::VectorF& position = subNode.getPosition( );
		subNode.setPosition( position( 0 ) - x / 2., position( 1 ) );
		computeBBox( subNode, bboxes );
	}

	// Force bounding box computation and caching
	computeBBox( node, bboxes );
}

bool	ChanTree::findLeafTrees( Node& node, Node::Set& leafTrees )
{
	if ( node.getOutEdges( ).size( ) == 0 )
		return true;

	bool leaf = true;
	Node::List outNodes; node.collectOutNodes( outNodes );
	foreach ( Edge* edge, node.getOutEdges( ) )
	{
		if ( findLeafTrees( edge->getDst( ), leafTrees ) == false )
			leaf = false;
	}
	if ( leaf )		// All subnode are leaf nodes, so we are at the root of a leaf subtree
		leafTrees.insert( &node );

	return false;
}

void	ChanTree::transformPositions( QGraphicsScene* scene, Node& node, VectorF current, QMap< Node*, QRectF >& bboxes, Node::Set& leafTrees )
{
	VectorF position = node.getPosition( ) + current;
	node.setPosition( position );

	if ( bboxes.contains( &node ) /*&& node.getOutDegree( ) == 0*//* && leafTrees.contains( &node )*/ )
	{
		QRectF bbox = bboxes[ &node ];
		bbox.translate( position( 0 ), position( 1 ) );
		if ( scene != 0 )
		{
			QGraphicsRectItem* item = new QGraphicsRectItem( bbox, 0, scene );
			QColor colors[ 4 ];
			colors[ 0 ] = Qt::blue;
			colors[ 1 ] = Qt::black;
			colors[ 2 ] = Qt::green;
			colors[ 3 ] = Qt::red;
			item->setPen( colors[ rand( ) % 4 ] );
		}
	}

	foreach ( Edge* edge, node.getOutEdges( ) )
		transformPositions( scene, edge->getDst( ), position, bboxes, leafTrees );
}

QRectF	ChanTree::computeHorizontalBbox( Node::List& nodes, QMap< Node*, QRectF >& bboxes  )
{
	QRectF result( 0., 0., 0., 0. );
	double height = 0.;
	double width = 0.;
	foreach ( Node* node, nodes )
	{
		QRectF bbox = computeBBox( *node, bboxes );
		width += bbox.width( );
		if ( bbox.width( ) <= 0.01 )
			width += 70.;
		height = qMax( height, bbox.height( ) );
	}
	result.setWidth( width );
	result.setHeight( height );
	return result;
}

QRectF	ChanTree::computeBBox( Node& node, QMap< Node*, QRectF >& bboxes )
{
	VectorF current( 2 );
	current( 0 ) = 0.;
	current( 1 ) = 0.;
	return computeBBox( node, current, bboxes );
}

QRectF	ChanTree::computeBBox( Node& node, VectorF current, QMap< Node*, QRectF >& bboxes )
{
	// Use caching
	if ( bboxes.contains( &node ) )
		return bboxes[ &node ];

	if ( node.getOutDegree( ) == 0 )
	{
		QRectF localBox( 0., 0., 70., 50. );
		return localBox;
	}

	double left		= 0.;
	double top		= 0.;
	double right	= 70.;
	double bottom	= 50.;
	foreach ( Edge* edge, node.getOutEdges( ) )
	{
		Node& subNode = edge->getDst( );
		QRectF subBox = computeBBox( subNode, current, bboxes );
		subBox.translate( subNode.getPosition( )( 0 ), subNode.getPosition( )( 1 ) );

		left	= qMin( subBox.left( ), left );
		top		= qMin( subBox.top( ), top );
		right	= qMax( subBox.right( ), right );
		bottom	= qMax( subBox.bottom( ), bottom );
	}

	QRectF	result( 0., 0., 0., 0. );
	result.setLeft( left );
	result.setTop( top );
	result.setRight( right );
	result.setBottom( bottom );
	bboxes[ &node ] = result;		// Cache result
	return result;
}
//-----------------------------------------------------------------------------


} // ::qan
