/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canController.h
// \author	Benoit Autheman (authem_b@epita.fr)
// \date	2003 August 13
//-----------------------------------------------------------------------------


#ifndef canController_h
#define canController_h


// Qanava headers
#include "./qanNode.h"


// STD headers
#include <map>


// QT headers
#include <QObject>
#include <QAction>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QActionGroup>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	class GraphView;

	//! Manage complex view modification operations in an AbstractItemView object (for example, panning, selection, etc.).
	/*!
		\nosubgrouping
	*/
	class Controller : public QObject
	{
		Q_OBJECT

	public:

		class Manager : public QList< Controller* >
		{
		public:

			Manager( QObject* parent ) : _actionGroup( new QActionGroup( parent ) ) { }

			virtual ~Manager( ) { }

			virtual bool	keyPressEvent( QKeyEvent* e );

			virtual bool	mousePressEvent( QMouseEvent* e );

			virtual bool	mouseReleaseEvent( QMouseEvent* e );

			virtual bool	mouseMoveEvent( QMouseEvent* e );

			virtual bool	mouseDoubleClickEvent( QMouseEvent* e );

			virtual	bool	wheelEvent( QWheelEvent* e );

			void			registerController( Controller* controller );

			Controller*		getController( QString name );

		private:

			QActionGroup*	_actionGroup;
		};

		Controller( QString name, GraphView& GraphView );

		virtual	~Controller( ) { }

		virtual bool	keyPressEvent( QKeyEvent* e ) { return false; }

		virtual bool	mousePressEvent( QMouseEvent* e ) { return false; }

		virtual bool	mouseReleaseEvent( QMouseEvent* e ) { return false; }

		virtual bool	mouseMoveEvent( QMouseEvent* e ) { return false; }

		virtual bool	mouseDoubleClickEvent( QMouseEvent* e ) { return false; }

		virtual	bool	wheelEvent( QWheelEvent* e ) { return false; }

		QString			getName( ) { return _name; }

	protected:

		GraphView&		getGraphView( ) { return _GraphView; }

	private:

		GraphView&		_GraphView;

		QAction*			_action;

		QString				_name;

	protected:

		void				setAction( QAction* action ) { _action = action; }

	public:

		//! Get a custom action by name in this controller.
		virtual	QAction*	getAction( QString name ) { return 0; }

	public slots:

		QAction*			getAction( ) { return _action; }

	signals:

		//! Emmitted when the controller action (must) state changes.
		void				toggled( bool on );
	};



	//! .
	/*!
		\nosubgrouping
	*/
	class PanController : public Controller
	{
		Q_OBJECT

		/*! \name PanController Constructor/Destructor *///--------------------
		//@{
	public:

		//! PanController constructor with associed graphics view initialization.
		PanController( GraphView& GraphView );

		//! PanController virtual destructor.
		virtual ~PanController( ) { }
		//@}
		//---------------------------------------------------------------------


		//! .
		enum State
		{
			NONE		= 0,
			KEYBOARD	= 1,
			PAN			= 2,
			PANNING		= 8,
		};

		/*! \name Keyboard Navigation Management *///--------------------------
		//@{
	public:

		//! .
		virtual bool	keyPressEvent( QKeyEvent* e );

		//! Enable or disable keyboard navigation (with arrow keys).
		void			setKeyboardNavigation( bool state );

		//! Get the current keyboard navigation state.
		bool			getKeyboardNavigation( ) const { return _keyboardNavigation; }

	private:

		//! Keyboard navigation state.
		bool			_keyboardNavigation;

		//! Keyboard navigation intensity.
		double			_keyboardNavigationIntensity;
		//@}
		//---------------------------------------------------------------------



		/*! \name Panning and Zooming Management *///--------------------------
		//@{
	public:
		virtual bool	mousePressEvent( QMouseEvent* e );

		virtual bool	mouseReleaseEvent( QMouseEvent* e );

		virtual bool	mouseMoveEvent( QMouseEvent* e );

	public:

		State			getMode( ) const { return _mode; }

		State			getState( ) const { return _state; }

		void			setState( State state ) { _state = state; }

		void			setMode( State mode ) { _mode = mode; }

	protected slots:

		void			toggled( bool state );

	private:

		//! .
		QPointF				_start;

		State				_mode;

		State				_state;
		//@}
		//---------------------------------------------------------------------
	};


	//!
	/*!
		\nosubgrouping
	*/
	class ZoomWindowController : public Controller
	{
		Q_OBJECT

		/*! \name ZoomWindowController Constructor/Destructor *///-------------
		//@{
	public:

		//! ZoomWindowController constructor with associed graphics view initialization.
		ZoomWindowController( GraphView& GraphView );

		//! ZoomWindowController virtual destructor.
		virtual ~ZoomWindowController( ) { }
		//@}
		//---------------------------------------------------------------------

		//! .
		enum State
		{
			NONE		= 0,
			ZOOM		= 4,
			ZOOMING		= 16
		};

		/*! \name Panning and Zooming Management *///--------------------------
		//@{
	public:

		virtual bool	mousePressEvent( QMouseEvent* e );

		virtual bool	mouseReleaseEvent( QMouseEvent* e );

		virtual bool	mouseMoveEvent( QMouseEvent* e );

	public:

		State			getMode( ) const { return _mode; }

		State			getState( ) const { return _state; }

		void			setState( State state ) { _state = state; }

		void			setMode( State mode ) { _mode = mode; }

	protected slots:

		void			toggled( bool state );

	private:

		//! .
		QPointF				_start;

		//! Geometric item used to model the dashed zoom selection window .
		QGraphicsRectItem*	_zoomRectItem;

		State				_mode;

		State				_state;
		//@}
		//---------------------------------------------------------------------
	};


	//!
	/*!
		\nosubgrouping
	*/
	class ZoomController : public Controller
	{
		Q_OBJECT

		/*! \name ZoomController Constructor/Destructor *///-------------------
		//@{
	public:

		//! ZoomController constructor with associed graphics view initialization.
		ZoomController( GraphView& GraphView );

		//! ZoomController virtual destructor.
		virtual ~ZoomController( ) { }
		//@}
		//---------------------------------------------------------------------



		/*! \name Panning and Zooming Management *///--------------------------
		//@{
	public:

		virtual	bool		wheelEvent( QWheelEvent* e );

		virtual	QAction*	getAction( QString name );

	protected slots:

		void				zoomIn( bool state );

		void				zoomOut( bool state );

	private:

		QAction*			_actionZoomIn;

		QAction*			_actionZoomOut;
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // canController_h

