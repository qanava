/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanEdgeItem.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 February 08
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanNodeItem.h"
#include "./qanEdgeItem.h"


// QT headers
#include <QGraphicsScene>
#include <QPainter>


namespace qan {	// ::qan


/* Arrow Constructor/Destructor *///-------------------------------------------
EdgeItem::EdgeItem( Style::Manager& styleManager, Style* style, QGraphicsItem* parent, QGraphicsScene* scene,
					Edge& edge, NodeItem* src, NodeItem* dst ) :
	GraphItem( styleManager, style ),
	QGraphicsItem( parent, scene ),
	_edge( edge ),
	_src( src ),
	_dst( dst ),
	_hasArrow( true ),
	_arrowSize( 4 ),
	_lineType( Qt::SolidLine ),
	_lineWeight( 1 )
{
	setZValue( 1. );

	if ( src != 0 && dst != 0 )
	{
		src->addEdge( this );
		dst->addEdge( this );
		updateItem( );
	}

	setVisible( true );
}

EdgeItem::~EdgeItem( )
{
	// FIXME: segfault with node dtor
	/*if ( _src != 0 )
		_src->removeEdge( this );
	if ( _dst != 0 )
		_dst->removeEdge( this );*/
}

QRectF	EdgeItem::boundingRect( ) const
{
	if ( _src == 0 || _dst == 0 )
		return QRectF( );

	QPointF src = _src->mapToScene( _src->boundingRect( ).center( ) );
	QPointF dst = _dst->mapToScene( _dst->boundingRect( ).center( ) );

	QPointF topLeft( qMin( src.x( ), dst.x( ) ),		// tl=(minx, miny)
					 qMin( src.y( ), dst.y( ) ) );
	QPointF bottomRight( qMax( src.x( ), dst.x( ) ),    // br=(maxx, maxy)
						 qMax( src.y( ), dst.y( ) ) );

	QRectF br( 0., 0., qAbs( bottomRight.x( ) - topLeft.x( ) ), qAbs( bottomRight.y( ) - topLeft.y( ) ) );

	// Take the text label into account
	br = br.unite( _labelRect );

	// Take the arrow into account
	br.adjust( -_arrowSize, -_arrowSize, _arrowSize, _arrowSize );

	return br;
}

void	EdgeItem::updateItem( )
{
	if ( _src == 0 || _dst == 0 )
		return;

	QPointF srcF = _src->mapToScene( _src->boundingRect( ).center( ) );
	QPointF dstF = _dst->mapToScene( _dst->boundingRect( ).center( ) );
	QPointF topLeft( qMin( srcF.x( ), dstF.x( ) ),		// tl=(minx, miny)
					 qMin( srcF.y( ), dstF.y( ) ) );

	setPos( topLeft );  // setPos should have been called update on the old garbage area ?

	if ( getStyle( ) != 0 && getStyle( )->has( "linetype" ) )
	{
		_lineType = Qt::SolidLine;
		const QVariant& value = getStyle( )->get( "linetype" );
		if ( value.isValid( ) )
			_lineType = value.value< int >( );
	}
	if ( getStyle( ) != 0 && getStyle( )->has( "lineweight" ) )
	{
		_lineWeight = 1;
		const QVariant& value = getStyle( )->get( "lineweight" );
		if ( value.isValid( ) )
			_lineWeight = value.value< int >( );
	}
	if ( getStyle( ) != 0 && getStyle( )->has( "arrowsize" ) )
	{
		_arrowSize = 4;
		const QVariant& value = getStyle( )->get( "arrowsize" );
		if ( value.isValid( ) )
			_arrowSize = value.value< int >( );
	}

	QGraphicsItem::update( );
}

void	EdgeItem::paint( QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget )
{
	if ( _src == 0 || _dst == 0 )
		return;

	QPointF srcF = _src->mapToScene( _src->boundingRect( ).center( ) );
	QPointF dstF = _dst->mapToScene( _dst->boundingRect( ).center( ) );
	QLineF line( mapFromScene( srcF ), mapFromScene( dstF ) );

	QPen arrowPen( Qt::black, _lineWeight, ( Qt::PenStyle )_lineType, Qt::RoundCap, Qt::RoundJoin );

	if ( _edge.getLabel( ).length( ) > 0 )
	{
		QPointF middleF = QPointF( 15., 5. ) + srcF + ( dstF - srcF ) / 2;
		middleF = mapFromScene( middleF );
		painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
		_labelRect = painter->boundingRect( QRectF( ), Qt::AlignLeft | Qt::AlignVCenter, _edge.getLabel( ) );
		_labelRect.translate( middleF ); // Keep the label rect to compute the edge bbox
		painter->drawText( middleF, _edge.getLabel( ) );
	}

	if ( _hasArrow )
	{
		const double Pi = 3.141592653;
		double TwoPi = 2.0 * Pi;
		double angle = ::acos( line.dx( ) / line.length( ) );
		if ( line.dy( ) >= 0 )
			angle = TwoPi - angle;

		// Get the intersection between arrow and dst node
		QRectF br = mapFromItem( _dst, _dst->boundingRect( ) ).boundingRect( );

		// Test intersection with all borders
		QLineF top( br.topLeft( ), br.topRight( ) );
		QLineF right( br.topRight( ), br.bottomRight( ) );
		QLineF bottom( br.bottomLeft( ), br.bottomRight( ) );
		QLineF left( br.topLeft( ), br.bottomLeft( ) );

		QPointF i;
		if ( line.intersect( top, &i ) == QLineF::BoundedIntersection ||
			line.intersect( right, &i ) == QLineF::BoundedIntersection ||
			line.intersect( bottom, &i ) == QLineF::BoundedIntersection ||
			line.intersect( left, &i ) == QLineF::BoundedIntersection )
		{
			QLineF shortLine( mapFromScene( srcF ), i );
			painter->setPen( arrowPen );
			painter->drawLine( shortLine );

			painter->setRenderHint( QPainter::Antialiasing );
			painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
			painter->setBrush( QBrush( QColor( 0, 0, 0 ) ) );

			double angle = ::acos( line.dx( ) / line.length( ) );
			if ( line.dy( ) <= 0 )
				angle = TwoPi - angle;
			painter->save( );
			painter->translate( i );
			painter->rotate( angle * 180. / Pi );

			double arrowSize = _arrowSize;
			double arrowLength = _arrowSize * 2.;
			i = QPointF( -arrowLength - 1., 0. );
			QPolygonF poly;
			poly	<< QPointF( i.x( ), i.y( ) - arrowSize )
					<< QPointF( i.x( ) + arrowLength, i.y( ) )
					<< QPointF( i.x( ), i.y( ) + arrowSize ) << QPointF( i.x( ), i.y( ) - arrowSize );
			painter->drawPolygon( poly );
			painter->restore( );
		}
	}
	else
	{
		painter->setPen( arrowPen );
		painter->drawLine( line );
	}
}

void	EdgeItem::disconnectEdge( )
{
	_src = 0;
	_dst = 0;
}

void	EdgeItem::nodeDestroyed( )
{
	disconnectEdge( );
}
//-----------------------------------------------------------------------------

}	// ::qan

