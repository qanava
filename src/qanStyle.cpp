/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qarte software.
//
// \file	qanStyle.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 January 03
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanStyle.h"


// QT headers
#include <QColor>
#include <QVariant>
#include <QFont>
#include <QSet>


namespace qan { // ::qan


/* Style Manager Constructor/Destructor *///-----------------------------------
Style::Manager::Manager( ) :
	_empty( new Style( "empty" ) )
{

}

Style::Manager::~Manager( )
{
	if ( _empty != 0 )
		delete _empty;
	clear( );
}
//-----------------------------------------------------------------------------



/* Style Management *///-------------------------------------------------------
void	Style::Manager::clear( )
{
	QSet< Style* >	styles = QSet< Style* >::fromList( _objectStyleMap.values( ) );
	foreach ( Style* style, styles )
		delete style;
	_objectStyleMap.clear( );
	_typeStyleMap.clear( );
	clearImages( );
}

void	Style::Manager::setStyle( void* object, Style& style )
{
	if ( !_objectStyleMap.contains( object ) )
		_objectStyleMap.insert( object, &style );
}

Style*	Style::Manager::getStyle( void* object )
{
	return _objectStyleMap.value( object, 0 );
}

const Style*	Style::Manager::getStyle( const void* object ) const
{
	return _objectStyleMap.value( const_cast< void* >( object ), 0 );
}

void	Style::Manager::setStyle( int type, Style& style )
{
	// Set the style if the type don't already have a style
	if ( !_typeStyleMap.contains( type )  )
		_typeStyleMap.insert( type, &style );
}

Style*	Style::Manager::getStyle( int type )
{
	return _typeStyleMap.value( type, 0 );
}

const Style*	Style::Manager::getStyle( int type ) const
{
	return _typeStyleMap.value( type, 0 );
}
//-----------------------------------------------------------------------------


/* Image Caching Management *///-----------------------------------------------
/*!
	\return	A cached image loaded from the given filename. If fileName is invalid, the returned image can be queried for isNull().
 */
QImage		Style::Manager::getImage( QString fileName )
{
	NameImageMap::iterator imageIter = _nameImageMap.find( fileName );
	if ( imageIter != _nameImageMap.end( ) )
		return imageIter.value( );
	else
	{
		QImage image( fileName );
		if ( image.isNull( ) )
			return image;
		else
		{
			_nameImageMap.insert( fileName, image );
			return image;
		}
	}

	return QImage();
}

void	Style::Manager::clearImages( )
{
	_nameImageMap.clear( );
}
//-----------------------------------------------------------------------------



/* Attribute Management *///---------------------------------------------------
void	Style::add( QString name, QVariant variant )
{
	_nameValueMap.insert( name, variant );
	reset( );	// Force coherent Interview update whitout using (begin/end)Insert()
	emit modified( );
}

void	Style::remove( QString name )
{
	_nameValueMap.remove( name );
	_imageNames.removeAll( name );
	reset( );	// Force coherent Interview update whitout using (begin/end)Insert()
	emit modified( );
}

/*!
	\return	true if the attribute has been renamed, false otherwise (for example, the name was already used by another attribute).
 */
bool	Style::rename( QString name, QString newName )
{
	if ( has( newName ) )
		return false;

	QVariant value = get( name );
	remove( name );
	add( newName, value );
	reset( );	// Force coherent Interview update
	return true;
}

/*!
	\return true if a QVariant is registered under the given name, false otherwise.
 */
bool		Style::has( QString name ) const
{
	NameValueMap::const_iterator nameIter = _nameValueMap.find( name );
	return ( nameIter != _nameValueMap.end( ) );
}

/*!
	\return	A QVariant registered under a given name, or an invalid QVariant if the name is unknown.
 */
QVariant	Style::get( QString name )
{
	return _nameValueMap.value( name, QVariant( ) );
}

/*!
	\return	A const QVariant registered under a given name, or an invalid QVariant if the name is unknown.
 */
const QVariant	Style::get( QString name ) const
{
	/*QVariant result;
	NameValueMap::const_iterator nameIter = _nameValueMap.find( name );
	if ( nameIter != _nameValueMap.end( ) )
		result = nameIter->second;
	return result;*/
	return _nameValueMap.value( name, QVariant( ) );
}

void		Style::addColor( QString name, int r, int g, int b )
{
	QVariant value;
	value = QColor( r, g, b ); // QVariant does not support QColor in its ctor (non gui only types)
	add( name, value );
}

/*!
	\return	The color registered with name, an invalid Qcolor otherwise.
 */
QColor		Style::getColor( QString name ) const
{
	QColor color;
	const QVariant& value = get( name );
	if ( value.isValid( ) )
		color = value.value< QColor >( );
	return color;
}

void		Style::addIcon( QString name, QIcon& icon )
{
	QVariant value;
	value = icon; // QVariant does not support gui types in its ctor
	add( name, value );
}

QIcon		Style::getIcon( QString name ) const
{
	QIcon icon;
	const QVariant& value = get( name );
	if ( value.isValid( ) )
		icon = value.value< QIcon >( );
	return icon;
}

void		Style::addImage( QString name, QImage image )
{
	QVariant value;
	value = image; // QVariant does not support gui types in its ctor
	add( name, value );
}

void		Style::addImageName( QString name, QString fileName )
{
	add( name, QVariant( fileName ) );
	_imageNames.push_back( name );
}
bool		Style::hasImageName( QString name ) const
{
	return ( _imageNames.contains( name ) );
}

QString		Style::getImageName( QString name ) const
{
	if ( hasImageName( name ) ) // Check if name is an image name type
	{
		QVariant imageName = get( name );
		if ( imageName.type( ) == QVariant::String )
			return imageName.toString( );
	}
	return QString( );
}
//-----------------------------------------------------------------------------



/* Qt Model Interface *///-----------------------------------------------------
QVariant	Style::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid( ) )
		return QVariant( "index invalid" );

	QVariant d;

	if ( ( role == Qt::FontRole ) && ( index.column( ) == 0 ) )
	{
		QFont font;
		font.setWeight( QFont::Bold );
		d = font;
	}

	if ( role == Qt::DisplayRole )
	{
		if ( index.column( ) == 0 )
		{
			if ( index.row( ) == 0 )
				d =	QString( "name" );
			else
				d = _nameValueMap.keys( ).at( index.row( ) - 1 );
		}
		else if ( index.column( ) == 1 )
		{
			if ( index.row( ) == 0 )
				d =	getName( );
			else
				d = _nameValueMap.values( ).at( index.row( ) - 1 );
		}
	}

	return d;
}

bool			Style::setData( const QModelIndex& index, const QVariant& value, int role )
{
	if ( index.row( ) == 1 )
		return QAbstractListModel::setData( index, value, role );

	if ( role == Qt::EditRole && index.column( ) == 1 )	// Edit values
	{
		QString		k = _nameValueMap.keys( ).at( index.row( ) - 1 );
		QVariant	v = _nameValueMap.values( ).at( index.row( ) - 1 );
		if ( v.type( ) == value.type( ) )
		{
			v = value;
			_nameValueMap.insert( k, v );
			emit dataChanged( index, index );
			emit modified( );
			return true;
		}
	}
	if ( role == Qt::EditRole && index.column( ) == 0 )	// Edit names
	{
		if ( value.type( ) == QVariant::String )
		{
			QString k = _nameValueMap.keys( ).at( index.row( ) - 1 );
			if ( rename( k, value.toString( ) ) )			// Rename old name with the editor content and eventually validate the input
			{
				emit dataChanged( index, index );
				emit modified( );
				return true;
			}
		}
	}
	return QAbstractListModel::setData( index, value, role );
}

Qt::ItemFlags	Style::flags( const QModelIndex &index ) const
{
	if ( !index.isValid( ) )
		return Qt::ItemIsEnabled;

	if ( index.row( ) == 0 )
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;   // First row (style name) is not editable
	else
		return Qt::ItemIsEnabled | Qt::ItemIsEditable;	// Other rows are editable in both the name and value columns

	return Qt::ItemIsEnabled;
}

QVariant	Style::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		if ( section == 0 )
			return QString( "Property" );
		else if ( section == 1 )
			return QString( "Value" );
	}
	return QVariant( );
}

int	Style::rowCount( const QModelIndex& parent ) const
{
	return 1 + size( );
}

int	Style::columnCount( const QModelIndex& parent ) const
{
	return 2;
}
//-----------------------------------------------------------------------------


} // ::qan

