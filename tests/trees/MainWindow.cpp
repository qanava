//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanRepository.h"
#include "../../src/qanGrid.h"
#include "../../src/qanGraphView.h"
#include "../../src/qanGraphScene.h"
#include "../../src/qanTreeLayout.h"
#include "./MainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QHeaderView>
#include <QTextCursor>
#include <QDirModel>
#include <QToolButton>


using namespace qan;

void	generateBranch( Graph& _graph, Node& superNode, int depth, QProgressDialog* progress, QApplication* app, bool root = false )
{
	if ( --depth < 1 )
		return;

	int subNodeCount = 1 + ( rand( ) % 4 );
	app->processEvents( );
	if ( root )
		progress->setMaximum( subNodeCount );

	for ( int n = 0; n < subNodeCount; n++ )
	{
		if ( root )
		{
			app->processEvents( );
			progress->setValue( n );
			if ( progress->wasCanceled( ) )
				break;
		}

		QString label;
		label = "Node ";
		label += QString::number( _graph.getNodeCount( ) );
		Node* node = _graph.addNode( label );
		_graph.addEdge( superNode, *node );

		bool goDeeper = ( rand( ) % 3 );
		if ( goDeeper )
			generateBranch( _graph, *node, depth, progress, app );
	}
}

void	generateTree( Graph& _graph, QProgressDialog* progress, QApplication* app, int depth = 5 )
{
	Node* root = _graph.addNode( "Root" );
	if ( root != 0 )
		generateBranch( _graph, *root, depth, progress, app, true );
}

//-----------------------------------------------------------------------------
MainWindow::MainWindow( QApplication* application, QWidget* parent ) :
	QMainWindow( parent ),
	_application( application )
{
	setupUi( this );

	QHBoxLayout *hboxA = new QHBoxLayout( _frameA );
	hboxA->setMargin( 0 );
	QHBoxLayout *hboxB = new QHBoxLayout( _frameB );
	hboxB->setMargin( 0 );

	// Create a graph item view and generate a simple graph
	Graph* graph = new Graph( );
	_graphViewA = new qan::GraphView( _frameA, QColor( 255, 255, 255 ) );
	GridItem* grid = new GridCheckBoardItem( _graphViewA, palette( ).color( QPalette::Base ), palette( ).color( QPalette::AlternateBase ) );

	_graphViewB = new qan::GraphView( _frameB, QColor( 255, 255, 255 ) );
	new GridCheckBoardItem( _graphViewB, palette( ).color( QPalette::Base ), palette( ).color( QPalette::AlternateBase ) );

	QProgressDialog* progress = new QProgressDialog( "Generating graph...", "Cancel", 0, 100, this );
	progress->setWindowModality( Qt::WindowModal );
	progress->show( );
	generateTree( *graph, progress, _application, 7 );
	progress->close( );

	hboxA->addWidget( _graphViewA );
	_graphViewA->setGraph( *graph );
	_graphViewA->layoutGraph( );

	hboxB->addWidget( _graphViewB );
	_graphViewB->setGraph( *graph );
	_graphViewB->layoutGraph( );


	_graphViewA->setGraphLayout( new qan::HierarchyTree( QPointF( 120., 55. ), QPointF( 10., 10. ), Qt::Horizontal ) );
	_graphViewA->layoutGraph( );

	_graphViewB->setGraphLayout(  new qan::ChanTree( ) );
	_graphViewB->layoutGraph( );
}
//-----------------------------------------------------------------------------


