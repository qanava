//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanRepository.h"
#include "../../src/qanGraphView.h"
#include "../../src/qanGraphScene.h"
#include "../../src/qanGrid.h"
#include "../../src/ui/uiStyleView.h"
#include "canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QTableView>
#include <QHeaderView>
#include <QTimer>


using namespace qan;


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QWidget* parent ) :
	QMainWindow( parent )
{
	setupUi( this );

    QHBoxLayout *hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );
	hbox->setSpacing( 2 );

	// Setup zoom and navigation toolbars
	QToolBar* zoomBar = addToolBar( "Zooming" );
	QSlider* sldZoom = new QSlider( Qt::Horizontal, zoomBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	sldZoom->setMinimumWidth( 190 );
	zoomBar->addWidget( sldZoom );

	QToolBar* navigationBar = addToolBar( "Navigation" );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	// Generate a simple graph
	Graph* graph = new Graph( );
	qan::Repository* gmlRepository = new qan::GMLRepository( "styles_data.xml" );
	gmlRepository->load( graph );
	graph->generateRootNodes( );

/*	Graph* graph = new Graph( );
	Node& na = *graph->addNode( "Node A<br> <b>Multi line</b> and rich-<br> text <i>HTML</i> label test.", 2 );
	Node& nb = *graph->addNode( "Node B", 1 );
	graph->addEdge( na, nb );

	Node& nc = *graph->addNode( "Node C<br>With an <b>icon</b>!", 1 );
	Node& nd = *graph->addNode( "<b>Node D</b><br>With a too long description.", 1 );*/


	// Setup item views
	_graphView = new qan::GraphView( _frame, QColor( 170, 171, 205 ) );
	GridItem* grid = new GridCheckBoardItem( _graphView, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	hbox->addWidget( _graphView );


	// Configure node selection and tooltip popup rendering
	//connect( _graphView->getTooltipModifier( ), SIGNAL( nodeTipped(qan::Node*, QPoint) ), SLOT( showNodeTooltip(qan::Node*, QPoint) ) );
	//connect( _graphView->getSelectionModifier( ), SIGNAL( nodeSelected(qan::Node*, QPoint) ), SLOT( selectNode(qan::Node*, QPoint) ) );


	// Add canvas pan and zoom action to the navigation tobar
	_cbGridType = new QComboBox( navigationBar );
	_cbGridType->addItem( QIcon(), "<< Grid Type >>" );
	_cbGridType->addItem( QIcon(), "Regular" );
	_cbGridType->addItem( QIcon(), "Check Board" );
	connect( _cbGridType, SIGNAL(activated(int)), this, SLOT(gridChanged(int)));
	navigationBar->addWidget( _cbGridType );

	connect( sldZoom, SIGNAL( valueChanged(int) ), _graphView, SLOT( setZoomPercent(int) ) );
	navigationBar->setIconSize( QSize( 16, 16 ) );
	if ( _graphView->getAction( "zoom_in" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_in" ) );
	if ( _graphView->getAction( "zoom_out" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_out" ) );
	navigationBar->addSeparator( );
	if ( _graphView->getAction( "pan_controller" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "pan_controller" ) );
	if ( _graphView->getAction( "zoom_window_controller" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_window_controller" ) );


/*	Style* blueNode = new Style( "bluenode" );
	blueNode->addImageName( "backimage", "images/gradblue.png" );
	blueNode->addColor( "bordercolor", 0, 0, 0 );
	graph->getM( ).applyStyle( &na, blueNode );

	Style* greenNode = new Style( "greennode" );
	greenNode->addImageName( "backimage", "images/gradgreen.png" );
	greenNode->addColor( "bordercolor", 50, 100, 200 );
	greenNode->addT< int >( "fontsize", 14 );
	graph->getM( ).applyStyle( &nb, greenNode );

	Style* iconNode = new Style( "iconnode" );
	iconNode->addImageName( "backimage", "images/gradblue.png" );
	iconNode->addImageName( "icon", "images/icon.png" );
	iconNode->addColor( "bordercolor", 0, 255, 0 );
	graph->getM( ).applyStyle( &nc, iconNode );

	Style* orangeNode = new Style( "orangenode" );
	orangeNode->addImageName( "backimage", "images/gradorange.png" );
	orangeNode->addColor( "bordercolor", 50, 100, 200 );
	orangeNode->addT< int >( "maximumwidth", 125 );
	orangeNode->addT< int >( "maximumheight", 25 );
	orangeNode->addT< bool >( "hasshadow", true );
	orangeNode->addColor( "shadowcolor", 150, 150, 150 );
	greenNode->addT< int >( "shadowoffset", 6 );
	graph->getM( ).applyStyle( &nd, orangeNode );*/

	//treeView->setModel( graphModel );
	qan::Style* style = 0;
	if ( graph->getNodes( ).size( ) > 0 )
	{
		qan::Node* node = graph->getNodes( ).at( 0 );
		style = graph->getM( ).getStyleManager( ).getStyle( node );
	}

	if ( style != 0 )
	{
		qan::StyleEditor* styleEditor = new qan::StyleEditor( this, style );
		styleEditor->setMaximumWidth( 200 );
		hbox->addWidget( styleEditor );
	}

	//styleView->setModel( orangeNode );
	/*show( );
	resize( 400, 200 );
	updateGeometry( );
	_graphView->getGraphicsView( )->resize( 300, 300 );*/

	_graphView->setGraph( *graph );
	QTimer::singleShot( 0, this, SLOT( layoutGraph( ) ) );
}

void MainWindow::gridChanged( int index )
{
/*	GridItem* grid = 0;
	if ( index == 1 )
		grid = new GridItemRegularItem( 0, QColor( 170, 171, 205 ) );
	else if ( index == 2 )
		grid = new GridCheckBoardItem( 0, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	if ( grid != 0 && _graphView != 0 && _graphView->getCanvas( ) != 0 )
		_graphView->getCanvas( )->setGrid( grid );*/
}

void MainWindow::selectNode( qan::Node * node, QPoint p )
{
/*	if ( node != 0 )
	{
		std::string msg( "Node \"" );
		msg += node->getLabel( );
		msg += "\" selected.";
		QMessageBox::information( this, "Qanava Basic Test", msg.c_str( ) );
	}*/
}

void	MainWindow::showNodeTooltip( qan::Node* node, QPoint p )
{
/*	NodeItemInfoPopup* popup = new NodeItemInfoPopup( this );
	popup->popup( QString( node->getLabel( ).c_str( ) ), p );*/
}

void	MainWindow::layoutGraph( )
{
	_graphView->layoutGraph( );
}
//-----------------------------------------------------------------------------


