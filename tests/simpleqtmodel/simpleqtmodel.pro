TEMPLATE	=app
TARGET = test_simpleqtmodel
FORMS	= canMainWindow.ui
CONFIG	+= qt warn_on debug
DEFINES	+= QANAVA
LANGUAGE	= C++
QT += xml

SOURCES	+= canApp.cpp \
		   canMainWindow.cpp
HEADERS	+= canMainWindow.h

macx | unix {
  QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  INCLUDEPATH += ../../src
  DEFINES += QANAVA_UNIX
  LIBS += -L../../build -lqanava
}

win32 {
  INCLUDEPATH += ../../src
  OBJECTS_DIR = ./Debug
  LIBS	+= ../../build/qanava.lib
}


