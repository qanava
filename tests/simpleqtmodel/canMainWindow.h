//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


#ifndef canMainWindow_h
#define canMainWindow_h


// Qanava headers
#include "../../src/qanGraph.h"
#include "../../src/qanGraphView.h"
#include "ui_canMainWindow.h"


// QT headers
#include <QMainWindow>
#include <QTreeView>


//-----------------------------------------------------------------------------
//!
/*!
	\nosubgrouping
*/
		class MainWindow : public QMainWindow, public Ui::MainWindow
		{
			Q_OBJECT

		public:

			MainWindow( QApplication* application, QWidget* parent = 0 );

			virtual ~MainWindow( ) { }

		private:

			MainWindow( const MainWindow& );

			MainWindow& operator=( const MainWindow& );

			qan::GraphView*		_graphView;

			QTreeView*			_graphTreeView;

			QApplication*		_application;

			qan::Graph*			_graph;

			qan::Style*			_whiteNode;

		protected slots:

			void	layoutGraph( );

			void	removeNode( );

			void	addNode( );

			void	addEdge( );

			void	modifyNode( );
		};
//-----------------------------------------------------------------------------


#endif //

