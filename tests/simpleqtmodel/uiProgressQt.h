//-----------------------------------------------------------------------------
// This file is a part of the Qarte software.
//
// \file	uiProgressQt.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 July 08
//-----------------------------------------------------------------------------


#ifndef uiProgressQt_h
#define uiProgressQt_h


// LTM headers
#include "../../src/utl/utlProgress.h"


// QT headers
#include <qprogressdialog.h>


//-----------------------------------------------------------------------------
namespace qan { // ::qan
	namespace qui { // ::qan::qui

		//! Abstract class used to provide feedback about a task progression.
		/*!
			\nosubgrouping
		*/
		class ProgressQt : public qan::utl::Progress
		{
			/*! \name Progress Constructor/Destructor *///------------------------
			//@{
		public:

			//! ProgressQt constructor.
			ProgressQt( QWidget* widget, int maxProgress = 100 );

			//! ProgressQt destructor.
			virtual ~ProgressQt( );

		protected:

			//! ProgressQt empty private copy constructor.
			ProgressQt( const ProgressQt& progress ) : Progress ( progress ) { }
			//@}
			//-----------------------------------------------------------------



			/*! \name Views Management *///------------------------------------
			//@{
		public:

			//! .
			virtual bool	getCancel( );

			QProgressDialog* _progress;

		protected:

			//!
			virtual	void	updateProgress( int percent );
			//@}
			//-----------------------------------------------------------------
		};

	} // ::qan::qui
} // ::qan
//-----------------------------------------------------------------------------


#endif // uiProgressQt_h

