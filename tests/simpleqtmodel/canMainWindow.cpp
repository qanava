//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanGraphView.h"
#include "../../src/qanGrid.h"
#include "./canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QSplitter>

// STD headers
#include <sstream>


using namespace qan;


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QApplication* application, QWidget* parent ) :
	QMainWindow( parent ),
	_application( application )
{
	// Setup ui
	setupUi( this );
    QHBoxLayout* hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );

	// Setup zoom and navigation toolbars
	QToolBar* navigationBar = addToolBar( "Navigation" );
	navigationBar->setIconSize( QSize( 16, 16 ) );
	QAction* layoutGraph = navigationBar->addAction( QIcon( ), "Layout" );
	connect( layoutGraph, SIGNAL( triggered( ) ), this, SLOT( layoutGraph( ) ) );
	navigationBar->addSeparator( );

	QAction* addNode = navigationBar->addAction( QIcon( ), "Add Node" );
	connect( addNode, SIGNAL( triggered( ) ), this, SLOT( addNode( ) ) );
	QAction* removeNode = navigationBar->addAction( QIcon( ), "Remove Node" );
	connect( removeNode, SIGNAL( triggered( ) ), this, SLOT( removeNode( ) ) );
	QAction* modifyNode = navigationBar->addAction( QIcon( ), "Modify Node" );
	connect( modifyNode, SIGNAL( triggered( ) ), this, SLOT( modifyNode( ) ) );
	QAction* addEdge = navigationBar->addAction( QIcon( ), "Add Edge" );
	connect( addEdge, SIGNAL( triggered( ) ), this, SLOT( addEdge( ) ) );
	navigationBar->addSeparator( );

	// Generate a simple graph
	_graph = new Graph( );
	Node& na = *_graph->addNode( "Node A", 2 );
	Node& nb = *_graph->addNode( "Node B", 1 );
	_graph->addEdge( na, nb );

	Node& nc = *_graph->addNode( "Node C", 1 );
	_graph->addEdge( nb, nc );

	Node& nd = *_graph->addNode( "<b>Node D</b>", 1 );

	_whiteNode = new Style( "whitenode" );
	_whiteNode->addColor( "backcolor", 164, 206, 57 );

	// Display the dir model in a standard qt tree view
	_graphTreeView = new QTreeView( _frame );
	_graphTreeView->setAlternatingRowColors( true );
	_graphTreeView->setMaximumWidth( 130 );
	_graph->updateModels( );
	_graphTreeView->setModel( _graph );
	_graphTreeView->setSelectionMode( QAbstractItemView::ExtendedSelection );

	// Create a grah item view and display the dir model
	_graphView = new qan::GraphView( _frame );
	GridItem* grid = new GridCheckBoardItem( _graphView, palette( ).color( QPalette::Base ), palette( ).color( QPalette::AlternateBase ) );

	_graphView->applyStyle( &na, _whiteNode );
	_graphView->applyStyle( &nb, _whiteNode );
	_graphView->applyStyle( &nc, _whiteNode );
	_graphView->applyStyle( &nd, _whiteNode );

	_graphView->setGraph( *_graph );
	_graphView->setGraphLayout( new qan::UndirectedGraph( ) );
	_graphView->layoutGraph( );

	QSplitter* qSplitter = new QSplitter( _frame );
	qSplitter->addWidget( _graphTreeView );
	qSplitter->addWidget( _graphView );
	hbox->addWidget( qSplitter );

	QToolBar* zoomBar = addToolBar( "Zooming" );
	QSlider* sldZoom = new QSlider( Qt::Horizontal, zoomBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	sldZoom->setMinimumWidth( 190 );
	zoomBar->addWidget( sldZoom );

	connect( sldZoom, SIGNAL( valueChanged(int) ), _graphView, SLOT( setZoomPercent(int) ) );
	zoomBar->setIconSize( QSize( 16, 16 ) );
	if ( _graphView->getAction( "zoom_in" ) != 0 )
		zoomBar->addAction( _graphView->getAction( "zoom_in" ) );
	if ( _graphView->getAction( "zoom_out" ) != 0 )
		zoomBar->addAction( _graphView->getAction( "zoom_out" ) );
	zoomBar->addSeparator( );
	if ( _graphView->getAction( "pan_controller" ) != 0 )
		zoomBar->addAction( _graphView->getAction( "pan_controller" ) );
	if ( _graphView->getAction( "zoom_window_controller" ) != 0 )
		zoomBar->addAction( _graphView->getAction( "zoom_window_controller" ) );
}

void	MainWindow::layoutGraph( )
{
	_graphView->layoutGraph( );
}

void	MainWindow::removeNode( )
{
	if ( _graphTreeView->selectionModel( )->selection( ).size( ) != 0 )
	{
		const QModelIndexList& indexes = _graphTreeView->selectionModel( )->selection( ).indexes( );
		if ( indexes.begin( ) != indexes.end( ) )
		{
			QModelIndex id = *indexes.begin( );
			Node* node = _graph->getModelIndexNode( id );
			if ( node != 0 )
				_graph->removeNode( *node );
		}
	}
}

void	MainWindow::addNode( )
{
	Node* parentNode = 0;
	if ( _graphTreeView->selectionModel( )->selection( ).size( ) != 0 )
	{
		const QModelIndexList& indexes = _graphTreeView->selectionModel( )->selection( ).indexes( );
		if ( indexes.begin( ) != indexes.end( ) )
		{
			QModelIndex id = *indexes.begin( );
			parentNode = _graph->getModelIndexNode( id );
		}
	}

	{
		// Insert root node
		Node* node = _graph->insertNode( QString( "Node " ) + QString::number( _graph->getNodeCount( ) ), -1 );
		_graphView->applyStyle( node, _whiteNode );
		//_graphView->updateGraphStyles( );
	}
}

void	MainWindow::addEdge( )
{
	Node* srcNode = 0;
	Node* dstNode = 0;
	if ( _graphTreeView->selectionModel( )->selection( ).size( ) == 2 )
	{
		const QModelIndexList& indexes = _graphTreeView->selectionModel( )->selection( ).indexes( );
		QModelIndex srcId = indexes.at( 0 );
		srcNode = _graph->getModelIndexNode( srcId );
		QModelIndex dstId = indexes.at( 1 );
		dstNode = _graph->getModelIndexNode( dstId );
	}

	if ( srcNode != 0 && dstNode != 0 )
		_graph->insertEdge( *srcNode, *dstNode );
}

void	MainWindow::modifyNode( )
{
	Node* node = 0;
	if ( _graphTreeView->selectionModel( )->selection( ).size( ) != 0 )
	{
		const QModelIndexList& indexes = _graphTreeView->selectionModel( )->selection( ).indexes( );
		if ( indexes.begin( ) != indexes.end( ) )
		{
			QModelIndex id = *indexes.begin( );
			node = _graph->getModelIndexNode( id );
		}
	}

//	if ( node != 0 )
//		_graph->modifyNode( GraphViewnode, node->getLabel( ) + "_youpi!" );
}
//-----------------------------------------------------------------------------


