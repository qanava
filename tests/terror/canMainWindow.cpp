//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2006 August 15
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanRepository.h"
#include "../../src/qanGrid.h"
#include "../../src/qanSimpleLayout.h"
#include "../../src/qanGraphView.h"
#include "../../src/qanGraphScene.h"
#include "../../src/qanTreeLayout.h"
#include "../../src/ui/uiStyleView.h"
#include "../../src/ui/uiStyleModel.h"
#include "../../src/ui/uiNodesItemModel.h"
#include "canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QTableView>
#include <QHeaderView>
#include <QToolButton>
#include <QSplitter>


using namespace qan;


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QWidget* parent ) :
	QMainWindow( parent ),
	_cbLayoutType( 0 )
{
	setupUi( this );
	setVisible( true );

	// Setup zoom and navigation toolbars
	QToolBar* zoomBar = addToolBar( "Zooming" );
	QSlider* sldZoom = new QSlider( Qt::Horizontal, zoomBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	sldZoom->setMinimumWidth( 190 );
	sldZoom->resize( 150, 20 );
	zoomBar->addWidget( sldZoom );

	QToolBar* navigationBar = addToolBar( "Navigation" );
	navigationBar->setIconSize( QSize( 16, 16 ) );


	// Generate a simple graph
	Graph* graph = new Graph( );
	qan::Repository* gmlRepository = new qan::GMLRepository( "terror.xml" );
	gmlRepository->load( graph );
	graph->generateRootNodes( );

	// Setup item views
	_graphView = new qan::GraphView( _frame, QColor( 170, 171, 205 ) );
	GridItem* grid = new GridCheckBoardItem( _graphView, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	QSplitter* splitter = new QSplitter( Qt::Vertical, this );
	splitter->setHandleWidth( 2 );
	splitter->addWidget( _graphView );
	setCentralWidget( splitter );


	// Configure node selection and tooltip popup rendering
	//connect( _graphView->getTooltipModifier( ), SIGNAL( nodeTipped(qan::Node*, QPoint) ), SLOT( showNodeTooltip(qan::Node*, QPoint) ) );
	//connect( _graphView->getSelectionModifier( ), SIGNAL( nodeSelected(qan::Node*, QPoint) ), SLOT( selectNode(qan::Node*, QPoint) ) );


	// Add canvas pan and zoom action to the navigation tobar
	_cbGridType = new QComboBox( navigationBar );
	_cbGridType->addItem( QIcon(), "<< Grid Type >>" );
	_cbGridType->addItem( QIcon(), "Regular" );
	_cbGridType->addItem( QIcon(), "Check Board" );
	connect( _cbGridType, SIGNAL(activated(int)), this, SLOT(gridChanged(int)));
	navigationBar->addWidget( _cbGridType );

	connect( sldZoom, SIGNAL( valueChanged(int) ), _graphView, SLOT( setZoomPercent(int) ) );
	navigationBar->setIconSize( QSize( 16, 16 ) );
	if ( _graphView->getAction( "zoom_in" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_in" ) );
	if ( _graphView->getAction( "zoom_out" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_out" ) );
	navigationBar->addSeparator( );
	if ( _graphView->getAction( "pan_controller" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "pan_controller" ) );
	if ( _graphView->getAction( "zoom_window_controller" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_window_controller" ) );

	QToolBar* layoutBar = addToolBar( "Layout" );
	layoutBar->setIconSize( QSize( 16, 16 ) );

	_cbLayoutType = new QComboBox( layoutBar );
	_cbLayoutType->addItem( QIcon(), "<< Layout Type >>" );
	_cbLayoutType->addItem( QIcon(), "Random" );
	_cbLayoutType->addItem( QIcon(), "Concentric 90" );
	_cbLayoutType->addItem( QIcon(), "Concentric 45" );
	_cbLayoutType->addItem( QIcon(), "Colimacon log" );
	_cbLayoutType->addItem( QIcon(), "Hierarchy" );
	_cbLayoutType->addItem( QIcon(), "Graph" );
	layoutBar->addWidget( _cbLayoutType );

	QToolButton* layoutButton = new QToolButton( layoutBar );
	layoutButton->setText( "Layout" );
	layoutBar->addWidget( layoutButton );
	connect( layoutButton, SIGNAL( clicked() ), this, SLOT( layoutGraph() ) );

	Style* pilotStyle = new Style( "pilot" );
	pilotStyle->addImageName( "backimage", "images/gradgreen.png" );
	pilotStyle->addColor( "bordercolor", 0, 0, 0 );
	pilotStyle->addImageName( "icon", "images/pilot.png" );
	pilotStyle->addT< bool >( "hasshadow", true );

	Style* wtc1Style = new Style( "wtc1" );
	wtc1Style->addColor( "backcolor", 120, 140, 180 );
	wtc1Style->addColor( "bordercolor", 0, 0, 0 );
	wtc1Style->addImageName( "icon", "images/wtc.png" );

	Style* wtc2Style = new Style( "wtc2" );
	wtc2Style->addColor( "backcolor", 80, 120, 210 );
	wtc2Style->addColor( "bordercolor", 0, 0, 0 );
	wtc2Style->addImageName( "icon", "images/wtc.png" );

	Style* pentagonStyle = new Style( "pentagon" );
	pentagonStyle->addImageName( "backimage", "images/gradblue.png" );
	pentagonStyle->addImageName( "icon", "images/pentagon.png" );
	pentagonStyle->addColor( "bordercolor", 0, 0, 0 );

	Style* pennsylvaniaStyle = new Style( "pennsylvania" );
	pennsylvaniaStyle->addImageName( "backimage", "images/gradblue.png" );
	pennsylvaniaStyle->addColor( "bordercolor", 0, 0, 0 );


	// Affect specific style according to nodes attributes
	int pilotRole = graph->hasAttribute( "pilot" );
	int flightRole = graph->hasAttribute( "flight" );
	if ( pilotRole != -1 && flightRole != -1 )
	{
		Node::List::iterator nodeIter = graph->getNodes( ).begin( );
		for ( ; nodeIter != graph->getNodes( ).end( ); nodeIter++ )
		{
			Node* node = *nodeIter;

			QString* pilotAttr = node->getAttribute< QString >( pilotRole );
			if ( pilotAttr != 0 && *pilotAttr == "true" )
					_graphView->applyStyle( node, pilotStyle );

			QString* flightAttr = node->getAttribute< QString >( flightRole );
			if ( flightAttr != 0 )
			{
				if ( *flightAttr == "American Airlines Flight 11 (WTC1)" )
					_graphView->applyStyle( node, wtc1Style );
				else if ( *flightAttr == "United Airlines Flight 175 (WTC2)" )
					_graphView->applyStyle( node, wtc2Style );
				else if ( *flightAttr == "American Airlines Flight 77 (Pentagon)" )
					_graphView->applyStyle( node, pentagonStyle );
				else if ( *flightAttr == "United Airlines Flight 93 (Pennsylvania)" )
					_graphView->applyStyle( node, pennsylvaniaStyle );
			}
		}
	}

	_graphView->setGraph( *graph );
	_graphView->setGraphLayout( new qan::Random( ) );   // View will take care of layout destruction
	_graphView->layoutGraph( );

	ui::NodesItemModel* nodesItemModel = new ui::NodesItemModel( *graph, this );
	QTableView* nodesTableView = new QTableView( this );
	nodesTableView->setAlternatingRowColors( true );
	nodesTableView->horizontalHeader( )->resizeSections( QHeaderView::Fixed );  // Fixed
	nodesTableView->horizontalHeader( )->setStretchLastSection( true );
	nodesTableView->verticalHeader( )->resizeSections( QHeaderView::Fixed );
	nodesTableView->verticalHeader( )->setDefaultSectionSize( 20 );
	nodesTableView->verticalHeader( )->hide( );
	nodesTableView->setModel( nodesItemModel );
	nodesTableView->adjustSize( );

	splitter->addWidget( nodesTableView );
}

void MainWindow::gridChanged( int index )
{
/*	GridItem* grid = 0;
	if ( index == 1 )
		grid = new GridItemRegularItem( 0, QColor( 170, 171, 205 ) );
	else if ( index == 2 )
		grid = new GridCheckBoardItem( 0, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	if ( grid != 0 && _graphView != 0 && _graphView->getCanvas( ) != 0 )
		_graphView->getCanvas( )->setGrid( grid );*/
}

void MainWindow::selectNode( qan::Node * node, QPoint p )
{
/*	if ( node != 0 )
	{
		QString msg( "Node \"" );
		msg += node->getLabel( );
		msg += "\" selected.";
		QMessageBox::information( this, "Qanava Basic Test", msg.c_str( ) );
	}*/
}

void	MainWindow::showNodeTooltip( qan::Node* node, QPoint p )
{
/*	NodeItemInfoPopup* popup = new NodeItemInfoPopup( this );
	popup->popup( QString( node->getLabel( ).c_str( ) ), p );*/
}

void	MainWindow::layoutGraph( )
{
	Layout* layout = 0;
	switch ( _cbLayoutType->currentIndex( ) )
	{
	case 0:
		return;
		break;
	case 1:
		layout = new qan::Random( );
		break;
	case 2:   // "Concentric 90"
		layout = new qan::Concentric( 90., 60. );
		break;
	case 3:   // "Concentric 45"
		layout = new qan::Concentric( 45., 60. );
		break;
	case 4:   // "Colimacon log"
		layout = new qan::Colimacon( 15., 8. );
		break;
	case 5:
		layout = new qan::HierarchyTree( QPointF( 120., 55. ), QPointF( 10., 10. ), Qt::Horizontal );
		break;
	case 6:
		layout = new qan::UndirectedGraph( );
		break;
	default:
		return;
		break;
	}

	// Apply the settings if all needed parameters have been specified
	if ( layout != 0 && _graphView != 0 )
	{
		_graphView->setGraphLayout( layout );   // View will take care of layout destruction
		_graphView->layoutGraph( new QProgressDialog( "Laying out...", "Cancel", 1, 100, this ) );
	}
}
//-----------------------------------------------------------------------------


