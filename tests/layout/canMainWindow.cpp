//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanGraphView.h"
#include "../../src/qanGrid.h"
#include "../../src/qanTreeLayout.h"
#include "./canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QHeaderView>
#include <QTextCursor>
#include <QDirModel>
#include <QToolButton>


using namespace qan;


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QApplication* application, QWidget* parent ) :
	QMainWindow( parent ),
	_application( application )
{
	setupUi( this );

	// Setup zoom and navigation toolbars
	QToolBar* toolBar = addToolBar( "Tools" );

	_cbLayoutType = new QComboBox( toolBar );
	_cbLayoutType->addItem( QIcon(), "<< Layout Type >>" );
	_cbLayoutType->addItem( QIcon(), "Hierarchy" );
	_cbLayoutType->addItem( QIcon(), "Graph" );
	_cbLayoutType->addItem( QIcon(), "Random" );
	toolBar->addWidget( _cbLayoutType );

	QToolButton* layoutGraph = new QToolButton( toolBar );
	layoutGraph->setIcon( QIcon( "./images/layout.png" ) );
	layoutGraph->setText( "Layout" );
	layoutGraph->setAutoRaise( true );
	layoutGraph->setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
	connect( layoutGraph, SIGNAL( clicked( ) ), this, SLOT( layoutGraph( ) ) );
	toolBar->addWidget( layoutGraph );
	toolBar->addSeparator( );

	QHBoxLayout *hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );

	// Create a graph item view and generate a simple graph
	Graph* graph = new Graph( );
	_graphView = new qan::GraphView( _frame, QColor( 255, 255, 255 ) );
	GridItem* grid = new GridCheckBoardItem( _graphView, palette( ).color( QPalette::Base ), palette( ).color( QPalette::AlternateBase ) );

	qan::Style* blueNode = new qan::Style( "bluenode" );
	blueNode->addImageName( "backimage", "images/gradblue.png" );
	blueNode->addColor( "bordercolor", 0, 0, 0 );

	qan::Style* greenNode = new qan::Style( "greennode" );
	greenNode->addImageName( "backimage", "images/gradgreen.png" );
	greenNode->addColor( "bordercolor", 50, 100, 200 );

	qan::Style* orangeNode = new qan::Style( "centernode" );
	orangeNode->addImageName( "backimage", "images/gradorange.png" );
	orangeNode->addColor( "bordercolor", 50, 100, 200 );
	orangeNode->addT< int >( "fontsize", 12 );

	qan::Node* center = graph->addNode( "<b>Center</b>" );
	_graphView->applyStyle( center, orangeNode );

	qan::Node* n = graph->addNode( "Node 1" );
	graph->addEdge( *center, *n );
	_graphView->applyStyle( n, blueNode );

	qan::Node* m = graph->addNode( "Node 11" );
	graph->addEdge( *n, *m );
	_graphView->applyStyle( m, blueNode );

	n = graph->addNode( "Node 2" );
	graph->addEdge( *center, *n );
	_graphView->applyStyle( n, blueNode );

	m = graph->addNode( "Node 21" );
	graph->addEdge( *n, *m );
	_graphView->applyStyle( m, blueNode );

	n = graph->addNode( "Node 3" );
	graph->addEdge( *center, *n );
	_graphView->applyStyle( n, greenNode );

	m = graph->addNode( "Node 31" );
	graph->addEdge( *n, *m );
	_graphView->applyStyle( m, greenNode );

	n = graph->addNode( "Node 4" );
	graph->addEdge( *center, *n );
	_graphView->applyStyle( n, greenNode );

	m = graph->addNode( "Node 41" );
	graph->addEdge( *n, *m );
	_graphView->applyStyle( m, greenNode );

	m = graph->addNode( "Node 42" );
	graph->addEdge( *n, *m );
	_graphView->applyStyle( m, greenNode );


	// Add canvas pan and zoom action to the navigation tobar
	QSlider* sldZoom = new QSlider( Qt::Horizontal, toolBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	//sldZoom->setMinimumWidth( 190 );
	toolBar->addWidget( sldZoom );

	connect( sldZoom, SIGNAL( valueChanged(int) ), _graphView, SLOT( setZoomPercent(int) ) );
	toolBar->setIconSize( QSize( 16, 16 ) );

	if ( _graphView->getAction( "zoom_in" ) != 0 )
		toolBar->addAction( _graphView->getAction( "zoom_in" ) );
	if ( _graphView->getAction( "zoom_out" ) != 0 )
		toolBar->addAction( _graphView->getAction( "zoom_out" ) );
	toolBar->addSeparator( );
	if ( _graphView->getAction( "pan_controller" ) != 0 )
		toolBar->addAction( _graphView->getAction( "pan_controller" ) );
	if ( _graphView->getAction( "zoom_window_controller" ) != 0 )
		toolBar->addAction( _graphView->getAction( "zoom_window_controller" ) );

	hbox->addWidget( _graphView );

	_graphView->setGraph( *graph );
	_graphView->layoutGraph( );
}

void MainWindow::layoutGraph()
{
	switch ( _cbLayoutType->currentIndex( ) )
	{
	case 0:
		return;
		break;
	case 1:
		{
			//_layout = new qan::HierarchyTree( QPointF( 120., 55. ), QPointF( 10., 10. ), qan::HierarchyTree::HORIZONTAL );
			_layout = new qan::ChanTree( );
		}
		break;
	case 2:
		_layout = new qan::UndirectedGraph( );
		break;
	case 3:
		_layout = new qan::Random( );
		break;
	default:
		return;
		break;
	}

	// Apply the settings if all needed parameters have been specified
	if ( _layout != 0 && _graphView != 0 )
	{
		// Update the graph layout
		//qan::ProgressQt progress( _application, this );
		_graphView->setGraphLayout( _layout );   // View will take care of layout destruction
		_graphView->layoutGraph( );
	}
}
//-----------------------------------------------------------------------------


