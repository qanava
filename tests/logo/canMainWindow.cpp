//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanGraphView.h"
#include "../../src/qanGrid.h"
#include "./canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QHeaderView>
#include <QTextCursor>
#include <QDirModel>


using namespace qan;


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QWidget* parent ) :
	QMainWindow( parent )
{
	setupUi( this );

    QHBoxLayout *hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );

	// Generate a simple graph
	Graph* graph = new Graph( );

	Node& nq = *graph->addNode( "<b> Q</b>" );
	Node& na1 = *graph->addNode( "<b>a</b>" );
	graph->addEdge( nq, na1, 4.f );

	Node& nn = *graph->addNode( "<b>n</b>" );
	graph->addEdge( na1, nn, 3.f );

	Node& na2 = *graph->addNode( "<b>a</b>" );
	graph->addEdge( nn, na2, 2.f );

	Node& nv = *graph->addNode( "<b>v</b>" );
	graph->addEdge( na2, nv );

	Node& na3 = *graph->addNode( "a" );
	graph->addEdge( nv, na3 );

	// Setup item views
	qan::GraphView* graphView = new qan::GraphView( _frame );
	GridItem* grid = new GridCheckBoardItem( graphView, palette( ).color( QPalette::Base ), palette( ).color( QPalette::AlternateBase ) );
	hbox->addWidget( graphView );

	Style* greenNode = new Style( "greennode" );
	greenNode->addImageName( "backimage", "images/gradgreen.png" );
	greenNode->addColor( "bordercolor", 50, 100, 200 );
	greenNode->addT< int >( "fontsize", 18 );
	greenNode->addT< int >( "maximumwidth", 25 );
	graphView->applyStyle( &nq, greenNode );

	Style* iconNode = new Style( "iconnode" );
	iconNode->addImageName( "backimage", "images/gradblue.png" );
	iconNode->addColor( "bordercolor", 0, 255, 0 );
	iconNode->addT< int >( "maximumwidth", 20 );
	iconNode->addT< int >( "fontsize", 16 );
	graphView->applyStyle( &na1, iconNode );

	Style* orangeNode = new Style( "orangenode" );
	orangeNode->addImageName( "backimage", "images/gradorange.png" );
	orangeNode->addColor( "bordercolor", 50, 100, 200 );
	orangeNode->addT< int >( "fontsize", 14 );
	orangeNode->addT< int >( "maximumwidth", 15 );
	orangeNode->addT< int >( "maximumheight", 25 );
	graphView->applyStyle( &nn, orangeNode );
	graphView->applyStyle( &na2, orangeNode );
	graphView->applyStyle( &nv, orangeNode );

	Style* redNode = new Style( "rednode" );
	redNode->addImageName( "backimage", "images/gradred.png" );
	redNode->addColor( "bordercolor", 50, 100, 200 );
	redNode->addT< int >( "fontsize", 14 );
	redNode->addT< int >( "maximumwidth", 14 );
	graphView->applyStyle( &na3, redNode );

	graphView->setGraph( *graph );
	graphView->layoutGraph( );
}
//-----------------------------------------------------------------------------


