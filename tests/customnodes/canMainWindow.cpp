//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanGraphView.h"
#include "../../src/qanGrid.h"
#include "../../src/qanSimpleLayout.h"
#include "canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QMessageBox>

using namespace qan;

qan::Node* current;

QVariant	MyNode::itemChange( GraphicsItemChange change, const QVariant& value )
{
	if ( change == ItemPositionChange )
	{
		QPointF p = value.toPointF( );
		const qan::VectorF& np = getNode( ).getPosition( );
		//if ( p.x( ) != np( 0 ) && p.y( ) != np( 1 ) )
			getNode( ).setPosition( p.x( ), p.y( ) );
		updateEdges( );
		current = &getNode( );
	}
	return QGraphicsItem::itemChange( change, value );
}


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QWidget* parent ) :
	QMainWindow( parent )
{
	setupUi( this );
	current = 0;

    QHBoxLayout *hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );
	hbox->setSpacing( 2 );

	// Setup zoom and navigation toolbars
	QToolBar* zoomBar = addToolBar( "Zooming" );
	QSlider* sldZoom = new QSlider( Qt::Horizontal, zoomBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	sldZoom->setMinimumWidth( 190 );
	zoomBar->addWidget( sldZoom );

	QToolBar* navigationBar = addToolBar( "Navigation" );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	// Generate a simple graph
	Graph* graph = new Graph( );
	Node& n1 = *graph->addNode( "1", 2 );
	Node& n2 = *graph->addNode( "2", 2 );
	Node& n3 = *graph->addNode( "3", 2 );
	Node& n4 = *graph->addNode( "4", 2 );
	Node& n5 = *graph->addNode( "5", 2 );
	Node& n6 = *graph->addNode( "6", 2 );
	Node& n7 = *graph->addNode( "7", 2 );
	Node& n8 = *graph->addNode( "8", 2 );
	Node& n9 = *graph->addNode( "9", 2 );
	graph->addEdge( n1, n2 ); graph->addEdge( n2, n3 );
	graph->addEdge( n4, n5 ); graph->addEdge( n5, n6 );
	graph->addEdge( n7, n8 ); graph->addEdge( n8, n9 );

	graph->addEdge( n1, n4 ); graph->addEdge( n2, n5 ); graph->addEdge( n3, n6 );
	graph->addEdge( n4, n7 ); graph->addEdge( n5, n8 ); graph->addEdge( n6, n9 );


	// Setup item views
	_graphView = new qan::GraphView( _frame, QColor( 170, 171, 205 ) );
	GridItem* grid = new GridCheckBoardItem( _graphView, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	_graphView->setRenderHint( QPainter::Antialiasing );
	hbox->addWidget( _graphView );
	graph->registerGraphicNodeFactory( new MyNode::Factory( _graphView->getStyleManager( ) ) );


	// Add canvas pan and zoom action to the navigation tobar
	_cbGridType = new QComboBox( navigationBar );
	_cbGridType->addItem( QIcon(), "<< Grid Type >>" );
	_cbGridType->addItem( QIcon(), "Regular" );
	_cbGridType->addItem( QIcon(), "Check Board" );
	connect( _cbGridType, SIGNAL(activated(int)), this, SLOT(gridChanged(int)));
	navigationBar->addWidget( _cbGridType );

	connect( sldZoom, SIGNAL( valueChanged(int) ), _graphView, SLOT( setZoomPercent(int) ) );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	Style* orangeNode = new Style( "orangenode" );
	orangeNode->addImageName( "backimage", "images/gradorange.png" );
	orangeNode->addColor( "bordercolor", 50, 100, 200 );
	orangeNode->addT< int >( "maximumwidth", 125 );
	orangeNode->addT< int >( "maximumheight", 25 );
//	_graphView->applyStyle( &nd, *orangeNode );

	_graphView->setGraph( *graph );
	_graphView->setGraphLayout( new qan::Concentric( 45., 60. ) );   // View will take care of layout destruction
	_graphView->layoutGraph( );

	_layout = new qan::UndirectedGraph( );
	_timer = new QTimer( this );
	_timer->start( 100 );
	connect( _timer, SIGNAL(timeout()), this, SLOT( layoutGraph() ) );

}

void	MainWindow::selectNode( qan::Node * node, QPoint p )
{
	if ( node != 0 )
	{
		QString msg( "Node \"" );
		msg += node->getLabel( );
		msg += "\" selected.";

		QMessageBox::information( this, "Qanava Basic Test", msg );
	}
}

void	MainWindow::layoutGraph( )
{
	_graphView->layoutGraph( 0, _layout, 1, current );  // Perform 1 step of layout, do not modify the dragged node position
}
//-----------------------------------------------------------------------------

